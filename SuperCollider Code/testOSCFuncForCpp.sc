/*SynthDef(\hit, {
arg out, freq=440, prate=180, pwidth=0.5,
sustain=0.3, amp=0.1;
var env, u;
env = Env.linen(Rand(0.001, 0.003), 0.2, 0.3);
freq = freq;
u = SinOsc.ar(
freq,
amp
);
u = LPF.ar(u, 500, 0.5) * EnvGen.kr(env, doneAction:2);
Out.ar(out, Pan2.ar(u,u))
}).add;


 [1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3].scramble[0..rrand(8, 18)];
 TempoClock.default.tempo;
 (
 var notes;

 notes = "[69,80,68,75,90]".interpret;
// //notes = [69, 80, 68, 75, 90];
//
 p = Pbind(
// 	// the name of the SynthDef to use for each note
 	\instrument, \hit,//\singrain,
 	\amp, 0.2,
// 	// MIDI note numbers -- converted automatically to Hz
 	\midinote,Pseq(notes, 1),
// 	// rhythmic values
 	\dur,Pseq([0.5, 0.5, 0.5, 0.5], 1)
 );
 p.play();
 )*/

(

var notes, ai_player, hit_player, arrayConfigs, syStart, syConfig;
var window, slider, mySynth, ampTxt, top, top1, titleFilter, cutOffLabel, resLabel,
 resTxt,  sliderRes, top2, top3, valres, titleReverb, mixLabel, mixTxt, mixSlider,
top4, roomLabel, roomTxt, roomSlider, top5, dampLabel, dampTxt, dampSlider, titleEnv, top6,
titleAmp, volTxt, volSlider, ampLabel, osc1, osc2, osc3, osc4, topOsc, titleOSc;

var  on, off;
s.boot;
notes = Array.newClear(128);

~ampX = 0.5;
~cuttoffLPFX = 300;
~qLPFX = 0.01;
~mixReverbX = 0.25;
~roomReverbX = 0.9;
~dampReverbX = 0.9;
~oscX = 0;
~envX;

////////////////////////////// SYNTESIZERS /////////////////////////////////


(
(
SynthDef(\generalSin,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = SinOsc.ar(freq, 0, 1);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

(
SynthDef(\generalSaw,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = LFSaw.ar(freq);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

(
SynthDef(\generalTri,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = LFTri.ar(freq);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

(
SynthDef(\generalSqr,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = LFPulse.ar(freq);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

);
/////////////////////////////END SIYNTHESIZERS //////////////////////////////

SynthDef(\marimba, {arg out=0, amp=0.1, t_trig=1, freq=100, rq=0.006;
	var env, signal;
	var rho, theta, b1, b2;
	b1 = 1.987 * 0.9889999999 * cos(0.09);
	b2 = 0.998057.neg;
	signal = SOS.ar(K2A.ar(t_trig), 0.3, 0.0, 0.0, b1, b2);
	signal = RHPF.ar(signal*0.8, freq, rq) + DelayC.ar(RHPF.ar(signal*0.9, freq*0.99999, rq*0.999), 0.02, 0.01223);
	signal = Decay2.ar(signal, 0.4, 0.3, signal);
	DetectSilence.ar(signal, 0.01, doneAction:2);
	Out.ar(out, signal*(amp*0.4)!2);

}).add;


SynthDef(\hit, {
arg out, freq=440, prate=180, pwidth=0.5,
sustain=0.3, amp=0.15;
var env, u;
env = Env.linen(0.102, 0.02, 0.6, 0.6, \sine);
freq = freq;
u = LFSaw.ar(
freq,
amp
);
u = RLPF.ar(u, 300, 10, 3) * EnvGen.kr(env, doneAction:2);
Out.ar(out, Pan2.ar(u,u))
}).add;



SynthDef(\synthDefTest, {|out, gate=1, freq=440, amp = 0.1|
    // doneAction: 2 frees the synth when EnvGen is done
	var signal;
	signal = SinOsc.ar(freq) * EnvGen.kr(Env.asr(0.1 * amp, 0.3 * amp , 1.3 * amp), gate, doneAction:2);
	Out.ar(out, [signal, signal]);
}).add; // use store for compatibility with pattern example below

h = {|midiNote, veloc|

	var synth;

	synth = switch (~osc,
	0,   { \generalSin },
	1, { \generalSaw },
	2, { \generalTri },
	3, { \generalSqr }
	);

    notes[midiNote] = Synth(synth, [\freq, midiNote.midicps, \amp, veloc * 0.01 * ~amp,\cutOffLPF, ~cuttoffLPF, \rq,~qLPF ,
		\mix ,~mixReverb, \room, ~roomReverb, \damp, ~dampReverb, \env, v.value]);
	//notes[midiNote] = Synth(\synthDefTest, [\freq, midiNote.midicps,
	//\amp, veloc * 0.00315]);
	//0;
};

g = {|midiNote|
	notes[midiNote].release;
	0;};


ai_player = { |notes, durations|
p = Pbind(
	// the name of the SynthDef to use for each note
	\instrument, \default,//\singrain,
	\amp, 0.2,
	// MIDI note numbers -- converted automatically to Hz
	\midinote,Pseq(notes, 1),
	// rhythmic values
	\dur,Pseq(durations, 1)
);
p.play();
	0;
};

hit_player = { |notes, durations|
p = Pbind(
	// the name of the SynthDef to use for each note
	\instrument, \hit,//\singrain,
	\amp, 0.2,
	// MIDI note numbers -- converted automatically to Hz
	\midinote,Pseq(notes, 1),
	// rhythmic values
	\dur,Pseq(durations, 1)
);
p.play();
	0;
};

x = OSCFunc( { | msg, time, addr, port |
	var midiNote;
	TempoClock.default.tempo = (msg[2].asInt)/60;
	midiNote = msg[1].asInt;
	(


		p = Pbind(
			// the name of the SynthDef to use for each note
			\instrument, \marimba,
			\amp, 0.2,
			// MIDI note numbers -- converted automatically to Hz
			\midinote,Pseq([midiNote], 1),
			// rhythmic values
			\dur,1
		);
		p.play();
	)
}, '/metronome' );

y = OSCFunc( { | msg, time, addr, port |
	var midiNote, veloc;
	midiNote = msg[2].asInt;
	veloc = msg[3].asInt;
	(
	if(veloc > 0,
			{h.value(midiNote, veloc);},
			{g.value(midiNote);}
	);
	)
}, '/player' );

z = OSCFunc( { | msg, time, addr, port |
	var notes, durations;
	notes = msg[1].asString.interpret;
	durations = msg[2].asString.interpret;
	ai_player.value(notes, durations)
}, '/aiplayer' );

z = OSCFunc( { | msg, time, addr, port |
	var notes, durations;
	notes = msg[1].asString.interpret;
	durations = msg[2].asString.interpret;
	hit_player.value(notes, durations)
}, '/hitplayer' );


syStart = OSCFunc( { | msg, time, addr, port |
	var size;
	size = msg[1].asString.interpret;
	arrayConfigs =  Array.newClear(size);
}, '/syStart' );

/*syConfig = OSCFunc( { | msg, time, addr, port |
	var path, index, values;
	path = msg[1].asString.interpret;
	index = msg[2].asString.interpret;
	values = Object.readTextArchive(path);
	arrayConfigs[index] =  values;
		~oscX = values[0];
		~ampX = values[1];
		~cuttoffLPFX = values[2].linexp(0,1,300,15000);
		~qLPFX = values[3];
		~mixReverbX = values[4];
		~roomReverbX  = values[5];
		~dampReverbX = values[6];
		~envX = values[7]);
}, '/syConfig' );*/

(


~amp = 0.5;
~cuttoffLPF = 300;
~qLPF = 0.01;
~mixReverb = 0.25;
~roomReverb = 0.9;
~dampReverb = 0.9;
~osc = 0;

//w = Window(\envtest, Rect(800, 100, 400, 300));

//w.front;



window = Window("Simple Synthesizer", Rect(512, 64, 360, 630));

Button(window, Rect(0, 0, 50, 30))
		.states_([["Save"]])
		.action_({
			GUI.dialog.savePanel({ |path|
				[
			~osc,
			volSlider.value,
			slider.value,
			sliderRes.value,
			mixSlider.value,
			roomSlider.value,
			dampSlider.value,
			v.value

		]
				.writeTextArchive(path)
			});
		});

Button(window, Rect(60, 0, 50, 30))
		.states_([["Load"]])
		.action_({
				var	values;
			File.openDialog("", { |path|
				values = Object.readTextArchive(path);
		switch (values[0],
	0,   { osc1.doAction; },
	1, { osc2.doAction;  },
	2, { osc3.doAction;  },
	3, { osc4.doAction;  }
	);
			volSlider.value = values[1];
		volSlider.doAction;
			slider.value = values[2];
		slider.doAction;
			sliderRes.value = values[3];
		sliderRes.doAction;
			mixSlider.value = values[4];
		mixSlider.doAction;
			roomSlider.value = values[5];
		roomSlider.doAction;
			dampSlider.value = values[6];
		dampSlider.doAction;
		v.env_ (values[7]);
			});
		});
/////////////////////////////// OSCILLATORS PARAMETERS /////////////////////////////////////

topOsc = 30 + 20;

titleOSc = StaticText(window, Rect(0, topOsc - 20, 220, 20));
	titleOSc.font = Font("Helvetica", 12).boldVariant;
	titleOSc.stringColor = Color.new255(139, 0, 139);
	titleOSc.align = \left;
titleOSc.string = "------------------ Oscillators ------------------";

osc1 = CheckBox.new( window, Rect( 0, topOsc, 116, 27 ), "Sine", 0, 0, 1, 0, 'linear');
osc1.value = true;
~osc = 0;
osc1.action = {
	osc1.value = true;
	osc2.value = false;
	osc3.value = false;
	osc4.value = false;
	~osc = 0;
};

osc2 = CheckBox.new( window, Rect( 80, topOsc, 116, 27 ), "Sawtooth", 0, 0, 1, 0, 'linear');
osc2.value = false;
osc2.action = {
	osc2.value = true;
	osc1.value = false;
	osc3.value = false;
	osc4.value = false;
	~osc = 1;
};

osc3 = CheckBox.new( window, Rect( 160, topOsc, 116, 27 ), "Triangle", 0, 0, 1, 0, 'linear');
osc3.value = false;
osc3.action = {
	osc3.value = true;
	osc1.value = false;
	osc2.value = false;
	osc4.value = false;
	~osc = 2;
};

osc4 = CheckBox.new( window, Rect( 240, topOsc, 116, 27 ), "Square", 0, 0, 1, 0, 'linear');
osc4.value = false;
osc4.action = {
	osc4.value = true;
	osc1.value = false;
	osc2.value = false;
	osc3.value = false;
	~osc = 3;
};

/////////////////////////////// VOLUME PARAMETERS /////////////////////////////////////

top = topOsc + 60;

titleAmp = StaticText(window, Rect(0, top - 30, 220, 20));
	titleAmp.font = Font("Helvetica", 12).boldVariant;
	titleAmp.stringColor = Color.new255(85, 26, 139);
	titleAmp.align = \left;
titleAmp.string = "------------ Volume: Amplitude ------------";


/////////////////////////////// VOLUME SLIDER /////////////////////////////////////


ampLabel = StaticText(window, Rect(0, top, 220, 20));
	ampLabel.font = Font("Helvetica", 12);
	ampLabel.stringColor = Color.black;
	ampLabel.align = \left;
ampLabel.string = "Volume:";

volTxt = StaticText(window, Rect(0, top, 320, 20));
	volTxt.font = Font("Helvetica", 12);
	volTxt.stringColor = Color.black;
	volTxt.align = \right;




volSlider = Slider(window, Rect(80, top, 182, 20));
volSlider.value = 0.5;
~amp = volSlider.value;
volTxt.string = ~amp.round(0.01).asString;
volSlider.action = {
	~amp = volSlider.value;
	volTxt.string = ~amp.round(0.01).asString;
};



/////////////////////////////// FILTER LPF PARAMETERS /////////////////////////////////////

top1 = top + 60;

titleFilter = StaticText(window, Rect(0, top1 - 30, 220, 20));
	titleFilter.font = Font("Helvetica", 12).boldVariant;
	titleFilter.stringColor = Color.new255(0, 139, 69);
	titleFilter.align = \left;
titleFilter.string = "--------------- Resonant LPF ---------------";


/////////////////////////////// FREQ SLIDER /////////////////////////////////////

cutOffLabel = StaticText(window, Rect(0, top1, 220, 20));
	cutOffLabel.font = Font("Helvetica", 12);
	cutOffLabel.stringColor = Color.black;
	cutOffLabel.align = \left;
cutOffLabel.string = "Cut Off Freq:";

ampTxt = StaticText(window, Rect(0, top1, 320, 20));
	ampTxt.font = Font("Helvetica", 12);
	ampTxt.stringColor = Color.black;
	ampTxt.align = \right;




slider = Slider(window, Rect(80, top1, 182, 20));
slider.value = 0.743525;
~cuttoffLPF = slider.value.linexp(0,1,300,15000);
ampTxt.string = ~cuttoffLPF.round(0.5).asString ++ " Hz";
slider.action = {
	var guival;
	~cuttoffLPF = slider.value.linexp(0,1,300,15000);
	ampTxt.string = ~cuttoffLPF.round(0.5).asString ++ " Hz";
};

/////////////////////////////// RES SLIDER /////////////////////////////////////

top2 = top1 + 30;

resLabel = StaticText(window, Rect(0, top2, 220, 20));
	resLabel.font = Font("Helvetica", 12);
	resLabel.stringColor = Color.black;
	resLabel.align = \left;
resLabel.string = "Resonance Q";

resTxt = StaticText(window, Rect(0, top2, 320, 20));
	resTxt.font = Font("Helvetica", 12);
	resTxt.stringColor = Color.black;
	resTxt.align = \right;


sliderRes = Slider(window, Rect(80, top2, 182, 20));
sliderRes.value = 0.5405;
valres = sliderRes.value.linexp(0,1,1,5000);
	~qLPF = 1 / valres;
	resTxt.string = valres.round(0.5).asString;
sliderRes.action = {
	valres = sliderRes.value.linexp(0,1,1,5000);
	~qLPF = 1 / valres;
	resTxt.string = valres.round(0.5).asString;
};


top3 = top2 + 60;

/////////////////////////////// FILTER REVERB PARAMETERS /////////////////////////////////////

titleReverb = StaticText(window, Rect(0, top3 - 20, 220, 20));
	titleReverb.font = Font("Helvetica", 12).boldVariant;
	titleReverb.stringColor = Color.blue;
	titleReverb.align = \left;
titleReverb.string = "--------------- Reverb ---------------";

/////////////////////////////// MIX SLIDER /////////////////////////////////////

mixLabel = StaticText(window, Rect(0, top3, 220, 20));
	mixLabel.font = Font("Helvetica", 12);
	mixLabel.stringColor = Color.black;
	mixLabel.align = \left;
mixLabel.string = "Mix:";

mixTxt = StaticText(window, Rect(0, top3, 320, 20));
	mixTxt.font = Font("Helvetica", 12);
	mixTxt.stringColor = Color.black;
	mixTxt.align = \right;

mixSlider = Slider(window, Rect(80, top3, 182, 20));
mixSlider.value = 0.25;
~mixReverb = mixSlider.value;
mixTxt.string = ~mixReverb.round(0.01).asString;
mixSlider.action = {
	~mixReverb = mixSlider.value;
	mixTxt.string = ~mixReverb.round(0.01).asString;
};

/////////////////////////////// ROOM SLIDER /////////////////////////////////////


top4 = top3 + 30;

roomLabel = StaticText(window, Rect(0, top4, 220, 20));
	roomLabel.font = Font("Helvetica", 12);
	roomLabel.stringColor = Color.black;
	roomLabel.align = \left;
roomLabel.string = "Room:";

roomTxt = StaticText(window, Rect(0, top4, 320, 20));
	roomTxt.font = Font("Helvetica", 12);
	roomTxt.stringColor = Color.black;
	roomTxt.align = \right;

roomSlider = Slider(window, Rect(80, top4, 182, 20));
roomSlider.value = 0.9;
~roomReverb = roomSlider.value;
roomTxt.string = ~roomReverb.round(0.01).asString;
roomSlider.action = {
	~roomReverb = roomSlider.value;
	roomTxt.string = ~roomReverb.round(0.01).asString;
};

/////////////////////////////// DAMP SLIDER /////////////////////////////////////


top5 = top4 + 30;

dampLabel = StaticText(window, Rect(0, top5, 220, 20));
	dampLabel.font = Font("Helvetica", 12);
	dampLabel.stringColor = Color.black;
	dampLabel.align = \left;
dampLabel.string = "Damp:";

dampTxt = StaticText(window, Rect(0, top5, 320, 20));
	dampTxt.font = Font("Helvetica", 12);
	dampTxt.stringColor = Color.black;
	dampTxt.align = \right;

dampSlider = Slider(window, Rect(80, top5, 182, 20));
dampSlider.value = 0.9;
~dampReverb = dampSlider.value;
dampTxt.string = ~dampReverb.round(0.01).asString;
dampSlider.action = {
	~dampReverb = dampSlider.value;
	dampTxt.string = ~dampReverb.round(0.01).asString;
};

/////////////////////////////// FILTER REVERB PARAMETERS /////////////////////////////////////

top6 = top5 + 30;

titleEnv = StaticText(window, Rect(0, top6, 220, 20));
	titleEnv.font = Font("Helvetica", 12).boldVariant;
	titleEnv.stringColor = Color.red;
	titleEnv.align = \left;
titleEnv.string = "--------------- Envelope ADSR ---------------";

/////////////////////////////// ENV WINDOW /////////////////////////////////////
v = HrEnvelopeView(window, Rect(0, top6 + 30, 360, 200)).env_(Env.new([0,1,0.75,0.75,0,0],[0.01,0.1,0.05,0.1,1]));

window.front;
);

)

/*x.free;
y.free;
z.free;*/
