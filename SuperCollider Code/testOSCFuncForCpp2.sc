/*SynthDef(\hit, {
arg out, freq=440, prate=180, pwidth=0.5,
sustain=0.3, amp=0.1;
var env, u;
env = Env.linen(Rand(0.001, 0.003), 0.2, 0.3);
freq = freq;
u = SinOsc.ar(
freq,
amp
);
u = LPF.ar(u, 500, 0.5) * EnvGen.kr(env, doneAction:2);
Out.ar(out, Pan2.ar(u,u))
}).add;


 [1,2,3, 1,2,3, 1,2,3, 1,2,3, 1,2,3].scramble[0..rrand(8, 18)];
 TempoClock.default.tempo;
 (
 var notes;

 notes = "[69,80,68,75,90]".interpret;
// //notes = [69, 80, 68, 75, 90];
//
 p = Pbind(
// 	// the name of the SynthDef to use for each note
 	\instrument, \hit,//\singrain,
 	\amp, 0.2,
// 	// MIDI note numbers -- converted automatically to Hz
 	\midinote,Pseq(notes, 1),
// 	// rhythmic values
 	\dur,Pseq([0.5, 0.5, 0.5, 0.5], 1)
 );
 p.play();
 )*/

/*
Env([0, 1, 0.25, 0, 0], [0.01, 0.2, 0.5, 0.5], \sine).plot;

Env.adsr(0.02, 0.2, 0.25, 1, 1, -4).test(2).plot;
Env.linen(0.01, 0.01, 0.2, 0.6, \sine).test.plot;
(
SynthDef(\synthe2, {
arg out, freq=440, prate=180, pwidth=0.5,
sustain=0.3, amp=0.15 , mix = 0.1, room = 0.25, damp = 0.5;
var env, u;
env = Env([0, 1, 0.15, 0, 0], [0.01, 0.02, 0.5, 0.5], \sine);
freq = freq;
u = LFTri.ar(
freq,0,
6 * amp
);
u = RLPF.ar(u, 125, 13, 3) * EnvGen.kr(env, doneAction:2);
Out.ar(out,

		 FreeVerb.ar(
            Pan2.ar(u,u),
            mix,
            room,
            damp
        ))
}).add;
)

(
var notes;
notes = [69, 73, 76, 78, 79,69, 73, 76, 78, 79,69, 73, 76, 78, 79];
 p = Pbind(
// 	// the name of the SynthDef to use for each note
 	\instrument, \synthe2,//\singrain,
 	\amp, 00.85,
// 	// MIDI note numbers -- converted automatically to Hz
 	\midinote,Pseq(notes, 1),
// 	// rhythmic values
 	\dur,Pseq([0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25], 1)
 );
 p.play();
)
*/


//Env([0, 1, 0.5, 0, 0], [0.01, 0.2, 0.5, 0.5], \sine).plot;
//Env.linen(0.01, 0.01, 0.2, 1, \sine).plot;
(

var notes, ai_player, hit_player;

s.options.maxNodes_(30000);
s.options.memSize_(131072);
s.boot;
notes = Array2D.new(200,128);

TempoClock.default = TempoClock.new(1, 0, 80/60, 16384 );

////////////////////////////// SYNTESIZERS /////////////////////////////////


(
(
SynthDef(\generalSin,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = SinOsc.ar(freq, 0, 1);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

(
SynthDef(\generalSaw,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = LFSaw.ar(freq);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

(
SynthDef(\generalTri,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = LFTri.ar(freq);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

(
SynthDef(\generalSqr,{ arg out, freq=440, amp=0.15, cutOffLPF=500, rq=1,
	mix = 0.25, room = 0.15, damp = 0.5;
	var osc, u, env, envctl, envGen;
	env = Env.newClear(6);
	envctl = \env.kr(env.asArray);//Env.new([0,1,0.75,0.75,0, 0],[0.1,0.1,0.05,0.1, 1]);
	osc = LFPulse.ar(freq);//*EnvGen.kr(env, doneAction:2);
	envGen = EnvGen.kr(envctl,doneAction:0);
	FreeSelf.kr(TDelay.kr(Done.kr(envGen),3));
	osc = RLPF.ar(osc, cutOffLPF, rq) *  envGen;
	osc = FreeVerb.ar(osc, mix, room, damp,mul: amp);
	osc = Compander.ar(osc,osc,
	    thresh: 0.05,
        slopeBelow: 1,
        slopeAbove: 0.5,
        clampTime: 0.01,
		        relaxTime: 0.01);
	Out.ar(out, Pan2.ar(osc,osc));
	}
).add;
);

);
/////////////////////////////END SIYNTHESIZERS //////////////////////////////

SynthDef(\marimba, {arg out=0, amp=0.1, t_trig=1, freq=100, rq=0.006;
	var env, signal;
	var rho, theta, b1, b2;
	b1 = 1.987 * 0.9889999999 * cos(0.09);
	b2 = 0.998057.neg;
	signal = SOS.ar(K2A.ar(t_trig), 0.3, 0.0, 0.0, b1, b2);
	signal = RHPF.ar(signal*0.8, freq, rq) + DelayC.ar(RHPF.ar(signal*0.9, freq*0.99999, rq*0.999), 0.02, 0.01223);
	signal = Decay2.ar(signal, 0.4, 0.3, signal);
	DetectSilence.ar(signal, 0.01, doneAction:2);
	Out.ar(out, signal*(amp*0.4)!2);

}).add;


SynthDef(\hit, {
arg out, freq=440, prate=180, pwidth=0.5,
sustain=0.3, amp=0.15;
var env, u;
env = Env.linen(0.102, 0.02, 0.6, 0.6, \sine);
freq = freq;
u = LFSaw.ar(
freq,
amp
);
u = RLPF.ar(u, 300, 10, 3) * EnvGen.kr(env, doneAction:2);
Out.ar(out, Pan2.ar(u,u))
}).add;


SynthDef(\synthe1, {
arg out, freq=440, prate=180, pwidth=0.5,
sustain=0.3, amp=0.15 , mix = 0.75, room = 0.75, damp = 0.1;
var env, u;
env = Env([0, 1, 0.5, 0, 0], [0.01, 0.2, 0.5, 0.5], \sine);
freq = freq;
u = LFSaw.ar(
freq,0,
6 * amp
);
u = RLPF.ar(u, 200, 5, 3) * EnvGen.kr(env, doneAction:2);
Out.ar(out,

		 FreeVerb.ar(
            Pan2.ar(u,u),
            mix,
            room,
            damp
        ))
}).add;


SynthDef(\synthe2, {
arg out, freq=440, prate=180, pwidth=0.5,
sustain=0.3, amp=0.15 , mix = 0.1, room = 0.25, damp = 0.5;
var env, u;
env = Env([0, 1, 0.15, 0, 0], [0.01, 0.02, 0.5, 0.5], \sine);
freq = freq;
u = LFTri.ar(
freq,0,
6 * amp
);
u = RLPF.ar(u, 125, 13, 3) * EnvGen.kr(env, doneAction:2);
Out.ar(out,

		 FreeVerb.ar(
            Pan2.ar(u,u),
            mix,
            room,
            damp
        ))
}).add;


SynthDef(\synthDefTest, {|out, gate=1, freq=440, amp = 0|
    // doneAction: 2 frees the synth when EnvGen is done
	var signal;
	signal = SinOsc.ar(freq) * EnvGen.kr(Env.asr(0.1, 0.3 * amp , 0.3 * amp), gate, doneAction:2);
	Out.ar(out, [signal, signal]);
}).add; // use store for compatibility with pattern example below

h = {|voice, midiNote, veloc, volume|
	notes[voice,midiNote] = Synth(\synthDefTest, [\freq, midiNote.midicps,
	\amp, volume * veloc  / 127.0]);// volume = 0.5
	//	\amp, 0]);
	0;
};

g = {|voice, midiNote|
	notes[voice,midiNote].release;
	0;};


ai_player = { |notes, durations, synthe, amp|
p = Pbind(
	// the name of the SynthDef to use for each note
	\instrument, synthe,//\singrain,
	\amp, amp,//0.2,
	// MIDI note numbers -- converted automatically to Hz
	\midinote,Pseq(notes, 1),
	// rhythmic values
	\dur,Pseq(durations, 1)
);
p.play();
	0;
};

hit_player = { |notes, durations|
p = Pbind(
	// the name of the SynthDef to use for each note
	\instrument, \hit,//\singrain,
	\amp, 0.2,//0.2,
	// MIDI note numbers -- converted automatically to Hz
	\midinote,Pseq(notes, 1),
	// rhythmic values
	\dur,Pseq(durations, 1)
);
p.play();
	0;
};

x = OSCFunc( { | msg, time, addr, port |
	var midiNote;

	TempoClock.default.tempo = (msg[2].asInt)/60;
	midiNote = msg[1].asInt;
	(


		p = Pbind(
			// the name of the SynthDef to use for each note
			\instrument, \marimba,
			\amp, 0.2,
			// MIDI note numbers -- converted automatically to Hz
			\midinote,Pseq([midiNote], 1),
			// rhythmic values
			\dur,1
		);
		p.play();
	)
}, '/metronome' );

y = OSCFunc( { | msg, time, addr, port |
	var midiNote, veloc, voice, volume;
	voice = msg[2].asInt;
	midiNote = msg[3].asInt;
	veloc = msg[4].asInt;
	volume = msg[5].asFloat;
	(
	if(veloc > 0,
			{h.value(voice, midiNote, veloc, volume);},
			{g.value(voice, midiNote);}
	);
	)
}, '/player' );

z = OSCFunc( { | msg, time, addr, port |
	var notes, durations;
	notes = msg[1].asString.interpret;
	durations = msg[2].asString.interpret;
	ai_player.value(notes, durations, msg[3].asString, msg[4].asFloat);
}, '/aiplayer' );

z = OSCFunc( { | msg, time, addr, port |
	var notes, durations;
	notes = msg[1].asString.interpret;
	durations = msg[2].asString.interpret;
	hit_player.value(notes, durations)
}, '/hitplayer' );


)

/*
x.free;
y.free;
z.free;
*/