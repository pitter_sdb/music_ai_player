//
//  kmeans.h
//  Kmeans
//
//  Created by Efrain Astudillo Vargas on 5/2/15.
//  Copyright (c) 2015 Efrain Astudillo Vargas. All rights reserved.
//

#ifndef Kmeans_kmeans_h
#define Kmeans_kmeans_h
#include <iostream>
#include <vector>
#include <map>
#include <math.h>
#include <stdlib.h>
#include "Eigen/Dense"

typedef Eigen::MatrixXd Data;        //!< Type Data Eigen::MatrixXd as container of data to be trained
typedef Eigen::MatrixXd Centroids;   //!< Type Data Eigen::MatrixXd as container of centroids of each cluster
typedef Eigen::VectorXd Identifiers; //!< Type Data Eigen::VectorXd as container of identifiers matching 
                                     // data with cluster identifiers.

/*!
 *  \brief K-Mean is one of the simplest unsupervised learning algorithms that solve the well 
 *  known clustering problem.
 *
 *  \class  Kmean
 *
 */
class Kmean {
public:
  /*!
   *  \brief Constructor
   *  \param _data The whole data to be clustered.
   *  \param _clusters. the numbers of groups which _data
   *  will be clustered.
   */
  Kmean( Data& _data, const unsigned _clusters) :
  mData( _data ), mClusters( _clusters )
  {
    mNeedComputeCentroidsAgain = true;
    mIdentifiers.resize( _data.rows() );
    mCentroids.resize(_clusters, _data.cols() );
  }
  
  /*!
   *  \brief Calculate centroids and set randomly each sample 
   *  to belongs to a particular centroid. (clustering)
   */
  void Initialize()
  {
    std::srand( ( unsigned int ) time( 0 ) );
    std::vector< unsigned > _temp;
    unsigned _random = 0;
    bool flag_exit = false;
    for ( unsigned i = 0; i < mClusters; ++i )
    {
      do{
        _random = rand() % mData.rows();
        for ( int j = i ; j >= 0; --j ) {
          if ( !_temp.empty() && _temp[j] == _random )
          {
            flag_exit = false;
            break;
          }
          flag_exit = true;
        }
        if ( flag_exit )
          _temp.push_back( _random );

      }while( !flag_exit );
    }
  
    // get 
    for (unsigned row = 0; row < mClusters ; ++row)
      mCentroids.row( row ) = mData.row( _temp[row] );
  }
  /*!
   *  \brief Return all data being trained
   *  \return
   */
  Data& GetData() const { return mData; }
  Centroids& GetCentroids() { return mCentroids; }
  Identifiers& GetIdentifiers() { return mIdentifiers; }
  /*! 
   *  \brief Return centroids for each cluster
   *  \return a vector of size mClusters.
   *  \see mClusters
   */
  Centroids& Compute() {
  
      // finisih condition is perform inside __searchNearest
    while ( mNeedComputeCentroidsAgain ) 
    {
      __searchNearest();
      __computeCentroids();
    }
    
    return mCentroids;
  }
  /*!
   *  \brief Indetify which group "_classify" data belongs
   *  \param _classify
   *  \param _centroids
   *  \return the identifier of the cluster belonging _classify
   */
  unsigned Classify(const Eigen::VectorXd& _classify , Centroids& _centroids ){
    Eigen::VectorXd::Index _idxMin;
      // find nearest neighbour
    ( _centroids.colwise() - _classify ).colwise().squaredNorm().minCoeff(&_idxMin);
    
    
    return (unsigned) _idxMin;
  }
private:
  Centroids mCentroids; //!< The Center of Mass (Geometric center) of each cluster indentified by its index
  Data &mData;          //!< Whole data training for clustering
  unsigned mClusters;   //!< number (quantity) of clusters to compute or to group
  
  bool mNeedComputeCentroidsAgain; //!< since centroids go changing iteratively, can
                                   // be the case some point had been classified wrongly. 
  unsigned mIterations;     //!< Maximum iteration to avoid infinite cycle computing clusters if
                            // mNeedComputeCentroidsAgain doesn't work. 
  Identifiers mIdentifiers; //!<  One dimensional array for identify each sample of data belongs to which cluster. ( contains just indexes of mData )
  
  /*!
   * \brief Looking for a nearest point to centroid and assign the identifier
   *  of the point belonging to this centroid.
   */
  void __searchNearest()
  {
    mNeedComputeCentroidsAgain = false;
    Eigen::VectorXd _nearest( mClusters );
    for ( unsigned _idx = 0; _idx < mData.rows() ; ++_idx )
    {
      for ( unsigned _idxClust = 0; _idxClust < mClusters ; ++_idxClust )
        _nearest(_idxClust) = ( mData.row( _idx ) - mCentroids.row( _idxClust ) ).squaredNorm();
    
      // find nearest neighbour
      //(mData.rowwise() - mCentroids.row( j )).rowwise().squaredNorm().minCoeff(&_idxMin);
        
     Eigen::VectorXd::Index _idxMin;
    /* double min = */_nearest.minCoeff( &_idxMin );// just want the index
    if ( mIdentifiers( _idx ) != _idxMin ) {
        mNeedComputeCentroidsAgain = true;
      std::cout << "Need compute centroids again " << std::endl;
    }
    
      mIdentifiers( _idx ) = _idxMin;
    }
  }
  /*!
   *  \brief Compute Centroids for each group of data which were previously 
   *  identified.
   */
  void __computeCentroids(){
    for ( unsigned _idxClust = 0; _idxClust < mClusters; ++_idxClust)
    {
      Eigen::VectorXd _tempCent = Eigen::VectorXd::Zero( mData.cols() );
        //_tempCent.Zero( mData.cols() );
      unsigned _count = 0;
      for (unsigned _idx = 0; _idx < mIdentifiers.size(); ++_idx) 
      {
        if ( mIdentifiers( _idx ) == _idxClust ) 
        {
          std::cout << "Identifier : " << mIdentifiers( _idx ) << "ClusterId: " 
          << _idxClust <<std::endl;
          std::cout << "Vector [ " << _idx << " ] : "<< mData.row(_idx)
          << std::endl;
          _tempCent += mData.row( _idx );
          std::cout << "Suma : "<< _tempCent << std::endl;
          ++_count;
        }
      }// end for identifiers
      mCentroids.row( _idxClust ) = _tempCent / _count;
      std::cout << "centroid #[ " << _idxClust << " ]\n"<< mCentroids.row( _idxClust) << std::endl;
    }// end for clusters
  }
};
/*!
 * \brief Load data from a file which will be used to train and create clusters
 *  \param _filename [in]. path to file containing the data
 *  \param _data [out]. Returning a matrix Eigen::MatrixXd of the data which was read.
 *  \return false if was some error processing the file, True otherwise
 */
static bool LoadData(const std::string& _filename, Data& _data){
  float X [][2] = {
    {1.0,2.0},  {0.5,0.8},  {0.2,0.1},  {1.7,4.8},
    {2.2,1.4},  {1.5,4.4},  {2.3,3.9},  {3.0,4.3},
    {3.8,1.9},  {8.7,8.0},  {9.7,8.2},  {5.7,9.5},
    {5.3,8.0},  {7.5,8.5},  {9.3,7.8},  {3.0,3.7}};
    // 16 rows and 2 columns (two features and 16 samples)
  _data.resize(16, 2);
  for ( unsigned row = 0; row < 16; ++row ) {
    for ( unsigned col = 0; col < 2; ++col ) {
      _data(row,col) = X [row][col];
    }
  }
  return true;
}

#endif
