//
//  main.cpp
//  Kmeans
//
//  Created by Efrain Astudillo Vargas on 5/2/15.
//  Copyright (c) 2015 Efrain Astudillo Vargas. All rights reserved.
//

#include <iostream>
#include "kmeans.h"

extern "C" int k_main( int argc, char** argv );

int main(int argc, char * argv[]) {
  // insert code here...
  
  argv[0] = (char*) malloc(sizeof(char)* 16);
  argv[1] = (char*) malloc(sizeof(char)* 16);
  strcpy(argv[0], "compress");
  strcpy(argv[1], "-v");

  argc = 2;
    //k_main(argc, argv);
  
  const unsigned clusters = 2;
    //load data
  Data data;
  LoadData("data.txt", data);
  // initialize parameters of Kmeans
  Kmean kmean(data, clusters);
  kmean.Initialize();
  Centroids &centroids = kmean.Compute();

  Eigen::VectorXd data_to_classify(2);
  data_to_classify << 9 , 2;
  
  unsigned cluster = kmean.Classify( data_to_classify , centroids );
  
  std::cout << "Data : "<<  data_to_classify <<" Classified to: " << 
  kmean.GetCentroids().row( cluster ) << std::endl;;
    return 0;
}
