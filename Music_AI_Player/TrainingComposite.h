#ifndef TrainingComposite_HEADER
#define TrainingComposite_HEADER

#include "AIMusicianTraining.h"
#include "KnowledgeRepresentation.h"

template <class Musician_Training_T, class Kw_Transformation_Strategy>
class TrainingComposite : public  AIMusicianTraining {
public:
	TrainingComposite(AIMusicianTraining& componentSource) :
		component1_(componentSource){}

	KnowledgeRepresentation Train();
	KnowledgeRepresentation Train(const KnowledgeRepresentation&);

private:
	AIMusicianTraining& component1_;
	Musician_Training_T component2_;
	Kw_Transformation_Strategy transformation_Knowledge_;
};

template <class Musician_Training_T, class Kw_Transformation_Strategy>
KnowledgeRepresentation TrainingComposite<Musician_Training_T, Kw_Transformation_Strategy>::Train() {
	KnowledgeRepresentation kw1 = component1_.Train();
	KnowledgeRepresentation kwTransform = transformation_Knowledge_.Apply(kw1);
	return component2_.Train(kwTransform);
};

template <class Musician_Training_T, class Kw_Transformation_Strategy>
KnowledgeRepresentation TrainingComposite<Musician_Training_T, Kw_Transformation_Strategy>::Train(const KnowledgeRepresentation& info) {
	KnowledgeRepresentation kw1 = component1_.Train();
	return info;
};

#endif