#ifndef MUSIC_COMPOSER_HEADER
#define MUSIC_COMPOSER_HEADER

#if defined(__WINDOWS_MM__)
#include <Windows.h>
#endif

#if defined(__MACOSX_CORE__)
#include <unistd.h>
#endif

#include <vector>
#include <thread>
#include <functional>

#include "RtMidi.h"  
#include "SynthesisManager.h"
#include "MidiUtilities.h"
#include "MidiDevices.h"
#include "AIMusicianComposer.h"
#include "AISynthesisPerformer.h"
#include "SynthesisEngine.h"


using namespace MidiUtilities;

struct MusicalPiece {
	std::vector<AIMusicianComposer*> composers;
	std::vector<AISynthesisPerformer*> synthesizers;
};

class MusicImproviser {
public:
	MusicImproviser(){
		keyNote_ = MusicOperation::MUSICAL_NOTE::C;
	}

	void Compose(std::vector<MidiLine> &input_set, bool clearAfterCompose, 
		vector<int>& globalNotes, vector<float>& globalDurations, vector<int>& notesNumbersPerThread);

  SynthesisManager & GetSynthManager() {return mSynthEngine.GetSynthManager(); } 
  
	void ClearMusicalLines(std::vector<MidiLine> &input_set);
	void GetMidiData(std::vector<MidiLine> &midiInfoSet, bool &doneFlag, std::vector<std::vector<MidiMessageInTime>>& midiMessages);
	void AddVoice(AIMusicianComposer* compositionStartegy, AISynthesisPerformer* synthesisStrategy){
		musicalPiece_.composers.push_back(compositionStartegy);
		musicalPiece_.synthesizers.push_back(synthesisStrategy);
	}

	void ClearVoices() {
		FreeMemory();
		musicalPiece_.composers.clear();
		musicalPiece_.synthesizers.clear();
	}

	void FreeMemory(){
		for (std::size_t i = 0; i < musicalPiece_.composers.size(); ++i) {
			delete musicalPiece_.composers[i];
		}

		for (std::size_t i = 0; i < musicalPiece_.synthesizers.size(); ++i) {
			delete musicalPiece_.synthesizers[i];
		}
	}

	void SetKeyNote(MusicOperation::MUSICAL_NOTE keyNote) {
		keyNote_ = keyNote;
	}

private:
  //SynthesisManager synthManager_;
  SynthesisEngine::SynthesisEngine mSynthEngine;
	MusicalPiece musicalPiece_;
	std::vector<CompositionOutput> compositionOutput_;
	MusicOperation::MUSICAL_NOTE keyNote_;	
};

void MusicImproviser::Compose(std::vector<MidiLine> &input_set, bool clearAfterCompose, vector<int>& globalNotes, vector<float>& globalDurations, vector<int>& notesNumbersPerThread) {
  
  MidiLine global_input_set;
  for (std::size_t i = 0; i < input_set.size(); ++i) {
    for (size_t j = 0; j < input_set[i].size(); j++) {
      global_input_set.push_back(input_set[i][j]);
    }
  }
  compositionOutput_.clear();
  compositionOutput_.resize(musicalPiece_.composers.size());
  //Executing threads
  
  std::vector<std::thread> threadsComposers;
  
  int Ncomposers = (int)musicalPiece_.composers.size();
  vector<vector<int>> globalThreadsNotes(Ncomposers);
  vector<vector<float>> globaThreadslDurations(Ncomposers);
  
  for (std::size_t c = 0; c < musicalPiece_.composers.size(); ++c) {
    
    threadsComposers.push_back(std::thread(&AIMusicianComposer::Apply,
                                           musicalPiece_.composers[c],
                                           keyNote_,
                                           std::ref(global_input_set),
                                           std::ref(compositionOutput_[c]),
                                           std::ref(globalThreadsNotes[c]),
                                           std::ref(globaThreadslDurations[c])));
  }
  //Join threads
  
  for (auto& th : threadsComposers) th.join();
  
  notesNumbersPerThread.clear();
  for (size_t i = 0; i < globalThreadsNotes.size(); ++i) {
    notesNumbersPerThread.push_back(globalThreadsNotes[i].size());
    for (size_t j = 0; j < globalThreadsNotes[i].size(); ++j) {
      globalNotes.push_back(globalThreadsNotes[i][j]);
      globalDurations.push_back(globaThreadslDurations[i][j]);
    }
  }
  
  //for (std::size_t c = 0; c < composers_.size(); ++c) {
  // //Use one thread for each composer
  // CompositionOutput output;
  // composers_[c]->Apply(input_set[i], output,globalNotes, globalDurations); 
  // compositionOutput_.push_back(output);
  //}
  
  for (std::size_t s = 0; s < musicalPiece_.synthesizers.size(); ++s) {
    mSynthEngine.PlayMelody(compositionOutput_[s], musicalPiece_.synthesizers[s]);
  }
  
  if (clearAfterCompose)
    ClearMusicalLines(input_set);
};

void MusicImproviser::GetMidiData(std::vector<MidiLine> &midiInfoSet, bool &doneFlag, std::vector<std::vector<MidiMessageInTime>>& midiMessages) {
	MidiDevices::GetMidiData(midiInfoSet, doneFlag, GetSynthManager(), midiMessages);
};


void MusicImproviser::ClearMusicalLines(std::vector<MidiLine> &input_set) {
	for (std::size_t i = 0; i < input_set.size(); ++i) {
		auto &line = input_set[i];
		std::vector<MidiInfo>().swap(line);
		line.clear();
	}
};


#endif
