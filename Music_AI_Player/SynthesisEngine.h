#ifndef SynthesisEngine_HEADER
#define SynthesisEngine_HEADER

#include <vector>
#include "SynthesisManager.h"
#include "KnowledgeRepresentation.h"

#ifdef __WINDOWS_MM__
  #include <WinUser.h>
	#include <tchar.h>
#elif __APPLE__
  #include <unistd.h>
#endif

namespace SynthesisEngine {
  class SynthesisEngine{
  private:
    SynthesisManager mSynthManager;
#if defined(__WINDOWS_MM__)
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
#elif __APPLE__
    static pid_t pid;
#endif
    
  public:
    void PlayMelody(const CompositionOutput& output, const AISynthesisPerformer * synthDef);
    void StartExternalEngine();
    void StopExternalEngine();
    SynthesisManager& GetSynthManager() { return mSynthManager; }
    
  };// end class
}// end namespace

void SynthesisEngine::SynthesisEngine::PlayMelody(const CompositionOutput& output, const AISynthesisPerformer * synthDef) {
  if (output.notes.size() != 0) {
    std::string noteStr("[");
    std::size_t i;
    for (i = 0; i < output.notes.size() - 1; ++i) {
      noteStr += std::to_string(output.notes[i]) + ",";
    }
    noteStr += std::to_string(output.notes[i]) + "]";
    
    std::string durStr("[");
    for (i = 0; i < output.durations.size() - 1; ++i) {
      durStr += std::to_string(output.durations[i]) + ",";
    }
    durStr += std::to_string(output.durations[i]) + "]";
    mSynthManager.SendOSCMessage("/aiplayer", noteStr.c_str(), durStr.c_str(), (synthDef->GetName()).c_str(), synthDef->GetVolume());
  }
}




void SynthesisEngine::SynthesisEngine::StartExternalEngine(){
		std::string newDir("..\\SuperCollider");
#if defined(__WINDOWS_MM__)
		const unsigned int BUFSIZE = 1024;
		TCHAR Buffer[BUFSIZE];
		DWORD dwRet;
		dwRet = GetCurrentDirectory(BUFSIZE, Buffer);
  
		SetCurrentDirectory(_tcsdup(TEXT("..\\SuperCollider")));
  
		//processSynth = WinExec("sclang.exe \"..\\SuperCollider Code\\testOSCFuncForCpp.sc\"", CREATE_NEW_CONSOLE);
  
		//STARTUPINFO si;
    //PROCESS_INFORMATION pi;
  
		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));
  
		cout << "testOSCFuncForCpp2" << endl;
  
		// Start the child process.
		if (!CreateProcess(NULL,   // No module name (use command line)
                       _tcsdup(TEXT("sclang.exe \"..\\SuperCollider Code\\testOSCFuncForCpp2.sc\"")),        // Command line
                       NULL,           // Process handle not inheritable
                       NULL,           // Thread handle not inheritable
                       FALSE,          // Set handle inheritance to FALSE
                       CREATE_NEW_CONSOLE,              // No creation flags
                       //CREATE_NEW_PROCESS_GROUP,              // No creation flags
                       NULL,           // Use parent's environment block
                       NULL,           // Use parent's starting directory
                       &si,            // Pointer to STARTUPINFO structure
                       &pi)           // Pointer to PROCESS_INFORMATION structure
        ) {
      printf("CreateProcess failed (%d).\n", GetLastError());
      
    }
		//Return directory
		SetCurrentDirectory(Buffer);
  // Wait until child process exits.
  //WaitForSingleObject(pi.hProcess, INFINITE);
#elif __APPLE__
  // =========================  APPLE  ==============================  
#endif
}

void SynthesisEngine::SynthesisEngine::StopExternalEngine() {
#ifdef __WINDOWS_MM__
		int retval = ::_tsystem(_T("taskkill /F /T /IM sclang.exe"));
		WaitForSingleObject(pi.hProcess, INFINITE);
  
#elif __APPLE__
  // =========================  APPLE  ============================
#endif
}


#endif