//Supercollider synthesis manager

#ifndef Synthesis_Manager_HEADER
#define Synthesis_Manager_HEADER


#include "../Music_AI_Player/Include/osc/OscOutboundPacketStream.h"
#include "../Music_AI_Player/Include/ip/UdpSocket.h"


#define ADDRESS "127.0.0.1"
#define PORT 57120

#define OUTPUT_BUFFER_SIZE 1024

class SynthesisManager {
public:
	SynthesisManager() : transmitSocket(IpEndpointName(ADDRESS, PORT)) {}
	
	void SendOSCMessage(const char * element, int int_msg0, int int_msg1, int int_msg2);
	void SendOSCMessage(const char * element, const char * msg0, int int_msg1, int int_msg2);
	void SendOSCMessage(const char * element, const char * msg0, int int_msg01, int int_msg1, int int_msg2);
	void SendOSCMessage(const char * element, const char * msg0, int int_msg01, int int_msg1, int int_msg2, float float_msg0);
	void SendOSCMessage(const char * element, const char * msg0, const char * msg1);
	void SendOSCMessage(const char * element, const char * msg0, const char * msg1, const char * msg2, float float_msg3);
private:
	UdpTransmitSocket transmitSocket;
};


void SynthesisManager::SendOSCMessage(const char * element, int int_msg0, int int_msg1, int int_msg2) {
	char buffer[OUTPUT_BUFFER_SIZE];
	osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

	p << osc::BeginBundleImmediate
		<< osc::BeginMessage(element)
		<< int_msg0 << int_msg1 << int_msg2 << osc::EndMessage
		/*<< osc::BeginMessage("/test2")
		<< true << 24 << (float)10.8 << "world" << osc::EndMessage*/
		<< osc::EndBundle;

	transmitSocket.Send(p.Data(), p.Size());
}

void SynthesisManager::SendOSCMessage(const char * element, const char * msg0, int int_msg1, int int_msg2) {
	char buffer[OUTPUT_BUFFER_SIZE];
	osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

	p << osc::BeginBundleImmediate
		<< osc::BeginMessage(element)
		<< msg0 << int_msg1 << int_msg2 << osc::EndMessage
		/*<< osc::BeginMessage("/test2")
		<< true << 24 << (float)10.8 << "world" << osc::EndMessage*/
		<< osc::EndBundle;

	transmitSocket.Send(p.Data(), p.Size());
}


void SynthesisManager::SendOSCMessage(const char * element, const char * msg0, int int_msg01, int int_msg1, int int_msg2) {
	char buffer[OUTPUT_BUFFER_SIZE];
	osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

	p << osc::BeginBundleImmediate
		<< osc::BeginMessage(element)
		<< msg0 << int_msg01 << int_msg1 << int_msg2 << osc::EndMessage
		/*<< osc::BeginMessage("/test2")
		<< true << 24 << (float)10.8 << "world" << osc::EndMessage*/
		<< osc::EndBundle;

	transmitSocket.Send(p.Data(), p.Size());
}

void SynthesisManager::SendOSCMessage(const char * element, const char * msg0, int int_msg01, int int_msg1, int int_msg2, float float_msg0) {
	char buffer[OUTPUT_BUFFER_SIZE];
	osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

	p << osc::BeginBundleImmediate
		<< osc::BeginMessage(element)
		<< msg0 << int_msg01 << int_msg1 << int_msg2 << float_msg0 << osc::EndMessage
		/*<< osc::BeginMessage("/test2")
		<< true << 24 << (float)10.8 << "world" << osc::EndMessage*/
		<< osc::EndBundle;

	transmitSocket.Send(p.Data(), p.Size());
}

void SynthesisManager::SendOSCMessage(const char * element, const char * msg0, const char * msg1) {
	char buffer[OUTPUT_BUFFER_SIZE];
	osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

	p << osc::BeginBundleImmediate
		<< osc::BeginMessage(element)
		<< msg0 << msg1 << osc::EndMessage
		/*<< osc::BeginMessage("/test2")
		<< true << 24 << (float)10.8 << "world" << osc::EndMessage*/
		<< osc::EndBundle;

	transmitSocket.Send(p.Data(), p.Size());
}

void SynthesisManager::SendOSCMessage(const char * element, const char * msg0, const char * msg1, const char * msg2,float float_msg3) {
	char buffer[OUTPUT_BUFFER_SIZE];
	osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);

	p << osc::BeginBundleImmediate
		<< osc::BeginMessage(element)
		<< msg0 << msg1 << msg2 << float_msg3 << osc::EndMessage
		/*<< osc::BeginMessage("/test2")
		<< true << 24 << (float)10.8 << "world" << osc::EndMessage*/
		<< osc::EndBundle;

	transmitSocket.Send(p.Data(), p.Size());
}



#endif