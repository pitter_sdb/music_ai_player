#ifndef SawSynthesizer_HEADER
#define SawSynthesizer_HEADER

#include "AISynthesisPerformer.h"

class  SawSynthesizer : public AISynthesisPerformer {
public:
	SawSynthesizer() : AISynthesisPerformer("synthe1", 0.1f){}
};

#endif