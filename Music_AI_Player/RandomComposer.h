#ifndef RANDOM_COMPOSER_HEADER
#define RANDOM_COMPOSER_HEADER

#include "OscOutboundPacketStream.h"
#include "UdpSocket.h"
#include <vector>
#include "MusicImproviser.h"
#include <iostream>
#include <random>
#include <numeric>
#include <algorithm>
#include "SynthesisManager.h"
#include "AIMusicianComposer.h"
#include "KnowledgeRepresentation.h"

class  RandomComposer : public AIMusicianComposer
{
public:
	//RandomComposer(){}
	void Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set, 
		CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations);
	CompositionOutput Compose() {
		CompositionOutput o;
		return o;
	}
private:
	std::default_random_engine generatorScramble_;	
	std::default_random_engine generatorNotesSize_;

	void Scramble(const std::vector<MidiInfo> &midi_set, std::vector<int> &notes_set, std::size_t size);
};

void RandomComposer::Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set, 
	CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations) {
	std::cout << "Composing: " << input_set.size() << std::endl;
	unsigned int minVal, maxVal;
	minVal = 4;
	maxVal = 8;

	if (input_set.size() < maxVal) {
		minVal = input_set.size() / 2;
		maxVal = input_set.size();
	}

	std::uniform_int_distribution<int> distributionNotesSize_(8, 18);
	Scramble(input_set, output.notes, distributionNotesSize_(generatorNotesSize_));

	std::vector<float> noteLenghts;
	noteLenghts.push_back(1.0f);
	noteLenghts.push_back(1.5f);


	std::uniform_int_distribution<int> distributionNotesLenghts_(0, 1);
	while (output.durations.size() < output.notes.size()) {
		output.durations.push_back(noteLenghts[distributionNotesLenghts_(generatorNotesSize_)]);
		float durationsSum = std::accumulate(output.durations.begin(), output.durations.end(), 0.0f);
		if (durationsSum >= 4) {
			auto maxDurIt = std::max_element(output.durations.begin(), output.durations.end());
			float maxDuration = *maxDurIt;
			output.durations.erase(maxDurIt);
			output.durations.push_back(maxDuration / 2.0f);
		}
	}

	globalNotes.insert(globalNotes.end(), output.notes.begin(), output.notes.end());
	globalDurations.insert(globalDurations.end(), output.durations.begin(), output.durations.end());
}

void RandomComposer::Scramble(const std::vector<MidiInfo> &midi_set, std::vector<int> &notes_set, std::size_t size) {
	if (midi_set.size() == 0) return;
	std::uniform_int_distribution<int> distribution(0, midi_set.size() - 1);
	for (std::size_t i = 0; i < size && i < midi_set.size(); ++i) {
		int indexRandom = distribution(generatorScramble_);  // generates number in the range 1..6 
		notes_set.push_back(midi_set[indexRandom].note);
	}
}

#endif