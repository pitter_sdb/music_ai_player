//
//  Music_AI_Player6.h
//  music_ai
//
//  Created by Efrain Astudillo Vargas on 8/2/15.
//  Copyright (c) 2015 Efrain Astudillo. All rights reserved.
//

#ifndef music_ai_Music_AI_Player6_h
#define music_ai_Music_AI_Player6_h
#include "cinder/app/AppNative.h"
#include "Slider.h"

#include "View.h"
#include "Label.h"
#include "Control.h"
#include "Slider.h"
#include "Button.h"
#include <vector>

class MusicApp : public ci::app::AppNative{
public:
  MusicApp();
  
  void setup()                                override;
  void draw()                                 override;
  void update()                               override;
  void shutdown()                             override;
  void prepareSettings( Settings *settings )  override;
  
  void setFuzzyControls();
  void setChain1Controls();
  void setChain2Controls();
  
  // ci::gl::Texture buildTextureText(const std::string & _text, int _font_size);
  cinder::DisplayRef getSecondDisplay();
  
private:
  ci::gl::Texture mImgTexture;
  boost::scoped_ptr<MusicAppManager> mMusicManager;
  Cinder::ControlRoom::ViewRef inspectorView;
  
  std::vector< std::vector< unsigned int > > mSignalNotes;
  cinder::gl::TextureFontRef mFont;
};

#endif
