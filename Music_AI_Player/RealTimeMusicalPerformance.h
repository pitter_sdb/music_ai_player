#ifndef RealTimeMusicalPerformance_HEADER
#define RealTimeMusicalPerformance_HEADER


#include <iostream>
#include <string>
#include <vector>

#include "red-black.h"

#include "midifile/MidiFile.h"
#include "midifile/Options.h"

#include "MidiUtilities.h"
#include "midi2melody.h"

//#include "GeneticAlgorithm.h"

#ifdef __WINDOWS_MM__
#include <tchar.h>

#endif

#include "MarkovTraining.h"

#include "MarkovKnowledgeRepresentation.h"
#include "FuzzyKnowledgeRepresentation.h"
#include "FuzzyTraining.h"
#include "BasicMidiKnowledgeRepresentation.h"


#include <thread>
#include <signal.h>

#include "Metronome.h"
#include "MusicImproviser.h"
#include "AIMusicianComposer.h"
#include "AISynthesisPerformer.h"
#include "RandomComposer.h"
#include "MarkovComposer.h"
#include "SawSynthesizer.h"
#include "TriSynthesizer.h"
#include "PianoSynthesizer.h"
#include "FixedComposer.h"
#include "SynthesisEngine.h"
#include "ComposerComposite.h"
#include "FuzzyComposer.h"






struct OutputThreadInfo {
	int notesNumber;
	AISynthesisPerformer* synthesizer;
};

class InputThreadInfo {
public:
  InputThreadInfo(int _index) : mIndex(_index), mNotesNumber(0) {}

	float GetVolume() const{
		return MidiDevices::input_volumes_[mIndex];
	}
	void SetVolume(float _volume) {
		MidiDevices::input_volumes_[mIndex] = _volume;
	}
  std::size_t GetNotesNumber() const { return mNotesNumber;}
  void        SetNotesNumber( std::size_t _notes_number )
  { 
    mNotesNumber = _notes_number; 
  }
private:
  std::size_t   mNotesNumber;
	std::size_t   mIndex;
};

class  RealTimeMusicalPerformance {
  private:
  SynthesisEngine::SynthesisEngine mSynthEngine;
  
	public:
		RealTimeMusicalPerformance(int bpm,  MusicOperation::MUSICAL_NOTE keyNote, void(*pointer_func_beatPerform) ()) :
			metronome_(bpm, pointer_func_beatPerform), mInitialized(false)
    {
			BPM_ = bpm;
			keyNote_ = keyNote;
		};

		~RealTimeMusicalPerformance() {
			musicImproviser_.FreeMemory();
			mSynthEngine.StopExternalEngine();
			for (size_t i = 0; i < kwFuzzyArray.size(); i++)
			{
        if( kwFuzzyArray[i] )
          delete kwFuzzyArray[i];
			}
		}

		void StartSynthesisEngine();
		void ExecutePerformance();
		void StopSynthesisEngine();
		void ClearData();		
		void Finish() {
			done_ = true;
			metronome_.Finish();
			//ExitProcess(processSynth);
		};

		void OnBeatPerform();

		void SavePerformanceInMidiFile(std::string fileName) {
			MidiUtilities::WriteMidiFileAllTracks(fileName, 960, BPM_, allMidiMessages_);
			//allMidiMessages_.clear();
		}

		void SetBPM(int bpm) {
			BPM_ = bpm;
			metronome_.SetTempo(bpm);
		}

		void SetKeyNote(MusicOperation::MUSICAL_NOTE keyNote) {
			keyNote_ = keyNote;
		}

		float GetEmotionValue(int emotionIndex) {
      if( !fuzzyComposer_ )
        return 0.0f;
			return fuzzyComposer_->GetEmotionsDegrees()[emotionIndex];
		}

		void SetEmotionValue(int emotionIndex, float value) {
      if ( !fuzzyComposer_ )
        return;
			NewEmotionsDegrees_ = fuzzyComposer_->GetEmotionsDegrees();
			NewEmotionsDegrees_[emotionIndex] = value;
			fuzzyComposer_->SetEmotionDegress(NewEmotionsDegrees_);			
		}

		std::vector<OutputThreadInfo> OutputThreadsInfoCollection;
		std::vector<InputThreadInfo> InputThreadsInfoCollection;

  bool isInitialized() const{ return mInitialized;}
	private:
		int Nvoices;
		int BPM_;		
		int counter_ = 0;
		bool done_;
		//unsigned int processSynth;
  
		bool mInitialized;
  
		Metronome metronome_;
		MusicImproviser musicImproviser_;
		MusicOperation::MUSICAL_NOTE keyNote_;

		std::vector<MidiLine> input_musical_lines_;
		std::vector<std::vector<MidiMessageInTime>> allMidiMessages_;
		std::vector<int> generalNotes_;
		std::vector<float> generalDurations_;

		

		//KNowledge
		MarkovKnowledgeRepresentation kwMarkov;
		FuzzyKnowledgeRepresentation kwFuzzy;
		std::vector<FuzzyKnowledgeRepresentation*> kwFuzzyArray;
		FuzzyComposer* fuzzyComposer_;

		std::vector<float> NewEmotionsDegrees_;

		
		void UpdateMetronome();
		void FinishPerformance();
		void UpdateEmotions();

		void PrepareComposer() {

			AIMusicianComposer* randomComposer1 = new RandomComposer;
			AISynthesisPerformer* sawSynth = new SawSynthesizer;
			AISynthesisPerformer* triSynth = new TriSynthesizer;

			/*AIMusicianComposer* fixedComposer = new FixedComposer;
			AISynthesisPerformer* pianoSynth = new PianoSynthesizer;*/

			kwFuzzy.Load();
			kwMarkov.Load();
			AIMusicianComposer* markovComposer = new MarkovComposer(kwMarkov);
			AIMusicianComposer* fuzzyComposer = new FuzzyComposer(&kwFuzzy);
			//Song 0 - NO taking into account
			//std::vector<float> emotionsDegrees{ 0, 30, 50, 95, 0 };
			//Song 1 *
      //std::vector<float> emotionsDegrees{ 0, 100, 10, 100, 0 };
			//Song 2
      //std::vector<float> emotionsDegrees{ 100, 50, 0, 0, 0 };
			//Song 3
			//std::vector<float> emotionsDegrees{ 100, 0, 0, 20, 80 };
			//Song 4
			//std::vector<float> emotionsDegrees{ 0, 100, 20, 100, 0 };
			//Song 5
			//std::vector<float> emotionsDegrees{ 0, 100, 100, 20, 0 };
			//Song 6
			//std::vector<float> emotionsDegrees{ 20, 0, 0, 80,100 };
			//Song 7
			//std::vector<float> emotionsDegrees{ 100, 0, 0, 100, 0 };
			//Song 8
			//std::vector<float> emotionsDegrees{ 80, 30, 0, 40 , 0 };
			//Song 9
			//std::vector<float> emotionsDegrees{ 0, 0, 100, 20, 75 };
			//Song 10
			//std::vector<float> emotionsDegrees{ 10,80, 0, 100, 10 };
			//Song 11 *
			//std::vector<float> emotionsDegrees{ 100, 80, 0, 0, 10 }; 
			//Song 12
			//std::vector<float> emotionsDegrees{ 0, 90, 0, 60, 0 }; 
			//Song 13
			//std::vector<float> emotionsDegrees{ 0, 0, 80, 20, 100 }; 
			//Song 14
			//std::vector<float> emotionsDegrees{ 0, 100, 50, 80, 10 };
			//Song 15
			//std::vector<float> emotionsDegrees{ 40, 10, 0,100, 50 };


			std::vector<float> emotionsDegrees{ 0, 100, 10, 100, 0 };

			fuzzyComposer_ = ((FuzzyComposer *)fuzzyComposer);
			fuzzyComposer_->SetEmotionDegress(emotionsDegrees);
			NewEmotionsDegrees_ = fuzzyComposer_->GetEmotionsDegrees();

			AISynthesisPerformer* pianoSynth = new PianoSynthesizer;

			musicImproviser_.ClearVoices();
			
			//*****musicImproviser_.AddVoice(fixedComposer, pianoSynth);
			//musicImproviser_.AddVoice(randomComposer1, sawSynth);
			musicImproviser_.AddVoice(markovComposer, sawSynth);
			musicImproviser_.AddVoice(markovComposer, triSynth);
			musicImproviser_.AddVoice(fuzzyComposer, pianoSynth);

			//FILL ThreadInfos;
			OutputThreadInfo outputThread;
      outputThread.notesNumber = 0;
			//MARKOV 1
			outputThread.synthesizer = sawSynth;
			OutputThreadsInfoCollection.push_back(outputThread);
			//MARKOV 2
			outputThread.synthesizer = triSynth;
			OutputThreadsInfoCollection.push_back(outputThread);
			//FUZZY
			outputThread.synthesizer = pianoSynth;
			OutputThreadsInfoCollection.push_back(outputThread);


			/*Nvoices = 55;
			for (size_t i = 0; i < Nvoices; i++){
				kwFuzzyArray.push_back(new FuzzyKnowledgeRepresentation);
				kwFuzzyArray[i]->Load();
				fuzzyComposer = new FuzzyComposer((kwFuzzyArray[i]));
				((FuzzyComposer *)fuzzyComposer)->SetEmotionDegress(emotionsDegrees);
				pianoSynth = new PianoSynthesizer;
				musicImproviser_.AddVoice(fuzzyComposer, pianoSynth);
			}	*/		
						
			musicImproviser_.SetKeyNote(keyNote_);


			//////////////////////////////////////////-------------------------------------------------------------

			/*
			kwMarkov.Load();
			AIMusicianComposer* markovComposer = new MarkovComposer(kwMarkov);
			AIMusicianComposer* randomComposer = new RandomComposer;
			ComposerComposite* composerComposite = new ComposerComposite;
			composerComposite->Add(markovComposer);
			composerComposite->Add(randomComposer);
			AISynthesisPerformer* pianoSynth = new PianoSynthesizer;

			musicImproviser_.ClearVoices();
			
			musicImproviser_.AddVoice(composerComposite, pianoSynth);
			
			keyNote_ = MusicOperation::MUSICAL_NOTE::C;
			musicImproviser_.SetKeyNote(keyNote_);*/
		}
		//void finish(int ignore);
};


void RealTimeMusicalPerformance::StartSynthesisEngine() {
	mSynthEngine.StartExternalEngine();
}

void RealTimeMusicalPerformance::StopSynthesisEngine(){
	mSynthEngine.StopExternalEngine();
}

void RealTimeMusicalPerformance::ExecutePerformance() {
	PrepareComposer();
	allMidiMessages_.clear();
	//Begin Metronome
	metronome_.Start();
	std::thread metronomeThread(&RealTimeMusicalPerformance::UpdateMetronome,this);
	std::cout << "Metronome Initialized" << std::endl;
	//End Metronome
	//Getting the data from keyboard
  mInitialized = true;
	musicImproviser_.GetMidiData(input_musical_lines_, done_, allMidiMessages_);
  std::cout << "Este código NUNCA se ejecuta" << std::endl;
}

void RealTimeMusicalPerformance::ClearData() {
	generalNotes_.clear();
	generalDurations_.clear();
}

int beatCounter = 0;
//float acc = 0;
//int counterMore200 = 0;
void RealTimeMusicalPerformance::OnBeatPerform() {
  beatCounter++;
  int midiNote;
	if ( counter_ == 0 ) midiNote = 72; 
  else midiNote = 69;

	musicImproviser_.GetSynthManager().SendOSCMessage("/metronome", midiNote, BPM_, 0);
	bool clearAfterCompose = true;

	//CLEAR general notes
  if( !generalNotes_.empty() )
    generalNotes_.clear();
  if( !generalDurations_.empty() )
    generalDurations_.clear();

  if( !InputThreadsInfoCollection.empty() )
    InputThreadsInfoCollection.clear();
  
	//COMPOSITION OPERATION
  size_t i;
  for (i = 0; i < input_musical_lines_.size(); ++i)
  {
	  InputThreadsInfoCollection.push_back(InputThreadInfo(i));
	  InputThreadsInfoCollection[i].SetNotesNumber(input_musical_lines_[i].size());
  }

  vector<int> notesNumbersPerThread;
	musicImproviser_.Compose(input_musical_lines_, clearAfterCompose, generalNotes_, generalDurations_, notesNumbersPerThread);

	++counter_;
	counter_ %= 4;
  
	for (i = 0; i < notesNumbersPerThread.size(); ++i)
  {
    
		OutputThreadsInfoCollection[i].notesNumber = notesNumbersPerThread[i];	
    //std::cout << "Notes Number: " << OutputThreadsInfoCollection[i].notesNumber <<std::endl;
	}
}


void RealTimeMusicalPerformance::UpdateMetronome() {
	metronome_.Update();
}


void RealTimeMusicalPerformance::FinishPerformance() {
	int finish = 0;
	while (finish == 0) {
		std::cin >> finish;
		if (finish != 0) {
			done_ = true;
			metronome_.Finish();
			std::cout << "FINISH" << std::endl;

		}
	}
}

void RealTimeMusicalPerformance::UpdateEmotions() {
	if (fuzzyComposer_ != nullptr) {		
		auto emotionsList = fuzzyComposer_->GetEmotionsDegrees();

		bool hasChanged = false;
		for (size_t i = 0; i < emotionsList.size(); ++i)	{
			auto emotionValue = emotionsList[i];
			if (emotionValue != NewEmotionsDegrees_[i]) {
				hasChanged = true;
				break;
			}
		}

		if (hasChanged) {
			cout << "Emotions Has changed ... !" << endl;
			fuzzyComposer_->SetEmotionDegress(NewEmotionsDegrees_);
		}
	}
}




#endif