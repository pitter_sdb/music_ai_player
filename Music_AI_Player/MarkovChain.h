//
//  MarkovChain.h
//  music_ai
//
//  Created by Efrain Astudillo on 2/24/15.
//  Copyright (c) 2015 Efrain Astudillo. All rights reserved.
//

#ifndef music_ai_MarkovChain_h
#define music_ai_MarkovChain_h
#include "AIComponent.h"
#include <cstdlib>
#include <ctime>

#include <random>

class MarkovChain {
public:
  MarkovChain(){
  }
  /*!
   *  \brief Configure the Random Generator and the size of the transition matrix
   *  The matrix will be initialized with zeros.
   */
  void Setup(int matrixSize){
    std::srand((unsigned int)std::time(0));
	AIComponent::GetInstance().SetMatrixSize(matrixSize);
    size_t rows_x_cols = AIComponent::GetInstance().GetNotes().size();
    mTransitionMatrix = Eigen::MatrixXd::Zero(rows_x_cols, rows_x_cols); // NxN matrix
    std::cout<< "Size of the notes is: [ " << rows_x_cols << " ]"<< std::endl;
    std::cout<< "Matrix dimension is : [ " << rows_x_cols << " x " << rows_x_cols << " ]"<< std::endl;
  }
  
  /*!
   *  \brief Training a Markov Chain. Will build the transition matrix.
   *  This methods will build a vector of vectors containing all secuence of notes.
   *  \param _filename. Path name of the archive which contains 
   *  all secuence of notes of a melody.
   */
  void Train(const char* _filename){
    std::vector< std::vector<int> > _secuence;
    AIComponent::GetInstance().ReadSecuenceFromFile(_filename, _secuence);
    
    Train( _secuence );
  }
  /*!
   *  \brief Training a Markov Chain. Will build the transition matrix.
   *  \param _vectors. Vector of vectors containing all secuence notes.
   */
  void Train(std::vector< std::vector<int> >& _vectors){
    size_t total_notes_in_secuence= 0;
    for (unsigned int secuence_idx = 0; secuence_idx < _vectors.size(); ++secuence_idx)
    {
      size_t vector_size = _vectors[secuence_idx].size();
      for (unsigned int note_idx = 0; note_idx < vector_size - 1; ++note_idx)
      {
        ++mTransitionMatrix(_vectors[secuence_idx][note_idx], _vectors[secuence_idx][note_idx + 1]);
      }
      total_notes_in_secuence += vector_size;
    }
    //AIComponent::GetInstance().Print(mTransitionMatrix);
    std::cout<< "Total Notas: " << total_notes_in_secuence <<std::endl;
    mTransitionMatrix /= total_notes_in_secuence;
    
    AIComponent::GetInstance().Normalize(mTransitionMatrix);
    //AIComponent::GetInstance().Print(mTransitionMatrix);
  }
  /*!
   *
   */
  int ChooseNextNote(int _current_note){
    double targetSum   = 0;
    double sum         = 0;
    int targetNote  = 0;
    double totalevents = 0;
    int i;
    int STATES = (int)AIComponent::GetInstance().GetNotes().size();
    _current_note = _current_note % STATES;  // remove any octave value
    for (i=0; i < STATES; i++) {
      totalevents += mTransitionMatrix(_current_note,i);
    }

	std::uniform_real_distribution<double> distributionNotesLenghts_(0, 1);

	targetSum = (distributionNotesLenghts_(generatorNotesSize_) * totalevents + 0.005);
	//do {
	auto x = mTransitionMatrix(_current_note, targetNote);
		while (targetNote < STATES &&
			sum + mTransitionMatrix(_current_note, targetNote) <= targetSum) {
			sum += mTransitionMatrix(_current_note, targetNote);
			targetNote++;
		}
		//	_current_note = (_current_note + 1) % STATES;
	//} while (targetNote < STATES && sum == 0);
    
	return targetNote;
  }
  
private:
  Eigen::MatrixXd mTransitionMatrix; //!< Transition matrix
  std::default_random_engine generatorNotesSize_;

  /*!
  *  \brief Get a random value between 0 - 1
  */
  double GetRandomValue() const {
	  return (double)std::rand() / RAND_MAX;
  }
  /*!
  *  \brief Print the transistion matrix
  */
  void Print() {
	  AIComponent::GetInstance().Print(mTransitionMatrix);
  }
};


#endif
