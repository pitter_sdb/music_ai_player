#ifndef PianoSynthesizer_HEADER
#define PianoSynthesizer_HEADER

#include "AISynthesisPerformer.h"

class  PianoSynthesizer : public AISynthesisPerformer {
public:
	PianoSynthesizer() : AISynthesisPerformer("default", 0.2f){}
};

#endif