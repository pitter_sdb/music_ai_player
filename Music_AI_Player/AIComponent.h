//
//  AIComponent.h
//  music_ai
//
//  Created by Efrain Astudillo on 2/23/15.
//  Copyright (c) 2015 Efrain Astudillo. All rights reserved.
//

#ifndef music_ai_AIComponent_h
#define music_ai_AIComponent_h
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>

#include "Eigen/Dense"

class AIComponent{
public:
  
  AIComponent() {
    
  }
  
  ~AIComponent(){
    if(_mInstance)
      delete _mInstance;
  }
  /*!
   *  \brief Get a static instance of the same Object.
   *  Which is based in Singleton pattern.
   */
  static AIComponent& GetInstance() {
    if (!_mInstance){
      _mInstance = new AIComponent;
    }
    return (*_mInstance);
  }
  /*!
   *  \brief Read all secuences of notes of a melody as vectors. 
   *  The File should be compose as many entries as you need. Each line in a file will
   *  be represented as a vector. So, All entries will be contained in another vector
   *  \param [in] _filename. The Path name of the file which will be read.
   *  \param [out] _secuence. The vector of vector representing all secuence of notes in the _filename
   */
  void ReadSecuenceFromFile(const char* _filename, std::vector< std::vector<int> > & _secuence){
    std::ifstream file(_filename);
    if (file.is_open())
    {
      std::string data;
      while (std::getline(file, data, '\n'))
      {
        std::vector<int> _vec;
        std::stringstream ss( data );
        while (std::getline(ss, data, ','))
        // get each secuence number and convert to int
          _vec.push_back( std::atoi( data.c_str() ) );
        
        _secuence.push_back( _vec );
      }
    }
    else
    {
      std::cout<< "File couldn't open: "<< _filename << std::endl;
    }
    
  }
  /*!
   *  \brief Normalize the transition matrix. This matrix should be regular. Which means..
   *  The sum of their values of each row should be equal to 1.
   *  \param _transition_matrix. The matrix with their probabilities.
   */
  void Normalize(Eigen::MatrixXd& _transition_matrix){
    for (unsigned int row = 0; row < _transition_matrix.rows() ; ++row) {
      float _sum = _transition_matrix.row( row ).sum();
      for (unsigned int col = 0; col < _transition_matrix.cols(); ++col) {
		  if (_sum != 0)
			_transition_matrix(row,col) /= _sum;
      }
    }
  }
  /*!
   *  \brief Print all values of the transition matrix in stdout
   *  \param _transition_matrix. The transition matrix
   */
  void Print(Eigen::MatrixXd& _transition_matrix){
	  std::cout << std::fixed << std::setprecision(2) << "Transition Matrix below: \n[ " << _transition_matrix << " ] " << std::endl;
  }
  /*!
   * \brief Get the representation of all midi notes in a vector.
   *  \return A vector with midi notes as integers.
   */
  std::vector<int>& GetNotes() {
    return mNotesNumber;
  }

  void SetMatrixSize(int matrixSize) {
	  mNotesNumber.clear();
	  for (unsigned int i = 0; i < matrixSize; ++i) {
		  mNotesNumber.push_back(i); // initialize all midi notes 0-127
	  }
  }
  
private:
  static AIComponent* _mInstance;

  std::vector<int> mNotesNumber;
  std::vector<std::string> mNotesNames;
};

 AIComponent *AIComponent::_mInstance = 0;

#endif
