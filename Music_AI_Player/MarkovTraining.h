#ifndef MarkovTraining_HEADER
#define MarkovTraining_HEADER

#include "AIMusicianTraining.h"
#include "AIComponent.h"
#include "KnowledgeRepresentation.h"
#include "MarkovKnowledgeRepresentation.h"
#include <iostream>
#include <vector>

class MarkovTraining : public  AIMusicianTraining {
public:
	MarkovTraining(std::vector<std::vector<int>>& setNotesToTrain, std::vector<std::vector<float>>& setDursToTrain) :
		chromaticNotesSet_(MusicOperation::GetTrainSetToChromatic(setNotesToTrain)), 
		fixedDurationsSet_(MusicOperation::GetTrainSetToFixedDurations(setDursToTrain)) {
		markovKnowledge_.TransitionMatrixNotes = Eigen::MatrixXd::Zero(notesSize_, notesSize_); 
		markovKnowledge_.TransitionMatrixDurations = Eigen::MatrixXd::Zero(durationsSize_, durationsSize_);
	}

	KnowledgeRepresentation* Train();
	KnowledgeRepresentation* Train(const KnowledgeRepresentation&);
private:
	int notesSize_ = 12;
	int durationsSize_ = fixedDurations.size();
	std::vector<std::vector<int>> chromaticNotesSet_;
	std::vector<std::vector<int>> fixedDurationsSet_;

	MarkovKnowledgeRepresentation markovKnowledge_;

	void TrainFromSet(const std::vector<std::vector<int>>& trainSet, Eigen::MatrixXd& transitionMatrix);
};

KnowledgeRepresentation* MarkovTraining::Train() {
	std::cout << "Markov training" << std::endl;
	TrainFromSet(chromaticNotesSet_, markovKnowledge_.TransitionMatrixNotes);
	TrainFromSet(fixedDurationsSet_, markovKnowledge_.TransitionMatrixDurations);
	return &markovKnowledge_;
}

KnowledgeRepresentation* MarkovTraining::Train(const KnowledgeRepresentation& info) {
	std::cout << "Markov training + knowledge" << std::endl;
	return &markovKnowledge_;
}

void MarkovTraining::TrainFromSet(const std::vector<std::vector<int>>& trainSet, Eigen::MatrixXd& transitionMatrix) {	

	size_t total_notes_in_secuence = 0;
	for (unsigned int secuence_idx = 0; secuence_idx < trainSet.size(); ++secuence_idx) {
		size_t vector_size = trainSet[secuence_idx].size();
		for (unsigned int note_idx = 0; note_idx < vector_size - 1; ++note_idx) {
			++transitionMatrix(trainSet[secuence_idx][note_idx], trainSet[secuence_idx][note_idx + 1]);
		}
		total_notes_in_secuence += vector_size;
	}
	//AIComponent::GetInstance().Print(mTransitionMatrix);
	std::cout << "Total Notas: " << total_notes_in_secuence << std::endl;
	std::cout << "Average Notas: " << 1.0f * total_notes_in_secuence / trainSet.size() << std::endl;
	transitionMatrix /= total_notes_in_secuence;

	AIComponent::GetInstance().Normalize(transitionMatrix);
	//AIComponent::GetInstance().Print(mTransitionMatrix);
}

#endif