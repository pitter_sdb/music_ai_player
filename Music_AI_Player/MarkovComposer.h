#ifndef MarkovComposer_HEADER
#define MarkovComposer_HEADER

#include "Eigen/Dense"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>
#include <vector>
#include "KnowledgeRepresentation.h"
#include "AIMusicianComposer.h"
#include "MidiUtilities.h"
#include "MarkovKnowledgeRepresentation.h"

using namespace MidiUtilities;

class  MarkovComposer : public  AIMusicianComposer {
public:

	MarkovComposer(Eigen::MatrixXd& transitionMatrixNotes, Eigen::MatrixXd& transitionMatrixDurs, std::vector<int>& startingNotes,
		int numberOfGeneratedNotes) :
		transitionMatrixNotes_(transitionMatrixNotes), TransitionMatrixDurations_(transitionMatrixDurs), startingNotes_(startingNotes)
	{
		Ngen_ = numberOfGeneratedNotes;
	}

	MarkovComposer(MarkovKnowledgeRepresentation& knowledge) : transitionMatrixNotes_(knowledge.TransitionMatrixNotes),
		TransitionMatrixDurations_(knowledge.TransitionMatrixDurations) {
	
	}

	CompositionOutput Compose() {
		CompositionOutput output;
		int currentState = startingNotes_[(rand() % startingNotes_.size())];
		/*std::cout << "GENERATE NOTES" << std::endl;
		for (int i = 0; i < Ngen_; i++) {
			std::cout << currentState << " ";
			currentState = ChooseNextItem(transitionMatrixNotes_, currentState);
			output.notes.push_back(currentState + 12 * ((rand() % 2) + 4));
		}
		currentState = fixedDurations[(rand() % fixedDurations.size())];;
		std::cout << "GENERATE DURATIONS" << std::endl;
		for (int i = 0; i < Ngen_; i++) {
			std::cout << currentState << " ";
			currentState = ChooseNextItem(TransitionMatrixDurations_, currentState);
			output.durations.push_back(fixedDurations[currentState % fixedDurations.size()]);
		}*/
		ComposeFromNote(currentState, output, Ngen_, 6);
		return output;
	}

	void Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set, 
		CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations) {

		std::vector<MidiInfo> normalizedInputSet(input_set);

		//Normalize notes 
		for (size_t i = 0; i < normalizedInputSet.size(); i++){
			normalizedInputSet[i].note = normalizedInputSet[i].note - keyNote;
		}

		unsigned int minVal, maxVal;
		minVal = 1;
		maxVal = normalizedInputSet.size();

		if (normalizedInputSet.size() > 0) {

			output.notes.clear();
			output.durations.clear();

			int currentState = normalizedInputSet[(rand() % normalizedInputSet.size())].note;
			ComposeFromNote(currentState, output, (rand() % (maxVal - minVal + 1) + minVal), 2.5);

			for (size_t i = 0; i < output.notes.size(); i++){
				output.notes[i] = output.notes[i] + keyNote;
			}

			globalNotes.insert(globalNotes.end(), output.notes.begin(), output.notes.end());
			globalDurations.insert(globalDurations.end(), output.durations.begin(), output.durations.end());
		}		
	}

private:
	Eigen::MatrixXd& transitionMatrixNotes_; 
	Eigen::MatrixXd& TransitionMatrixDurations_;
	std::vector<int> startingNotes_;	
	int Ngen_;
	std::default_random_engine generatorNotesSize_;

	void ComposeFromNote(int startingNote, CompositionOutput& output, int numToGen, float durationLimit) {
		int currentState = startingNote;
		int octaves = startingNote % 12;

		//octaves = 5 + rand() % (7 - 5 + 1);
		octaves = 5 + rand() % (5 - 5 + 1);


		//std::cout << "GENERATE NOTES" << std::endl;
		for (int i = 0; i < numToGen; i++) {
			//std::cout << currentState << " ";
			currentState = ChooseNextItem(transitionMatrixNotes_, currentState);
			//output.notes.push_back(currentState + 12 * ((rand() % 2) + 4) + keyNote);
			output.notes.push_back(currentState + 12 * octaves);
		}
		currentState = fixedDurations[(rand() % fixedDurations.size())];
		//std::cout << "GENERATE DURATIONS" << std::endl;
		for (int i = 0; i < numToGen; i++) {
			//std::cout << currentState << " ";
			currentState = ChooseNextItem(TransitionMatrixDurations_, currentState);
			auto duration = fixedDurations[currentState % fixedDurations.size()];
			if (duration > durationLimit)
				duration = durationLimit;
			output.durations.push_back(duration);
		}
	}

	int ChooseNextItem(const Eigen::MatrixXd& transitionMatrix, int _current_note) {
		double targetSum = 0;
		double sum = 0;
		int targetNote = 0;
		double totalevents = 0;
		int i;
		int STATES = transitionMatrix.rows();
		_current_note = _current_note % STATES;  // remove any octave value
		for (i = 0; i < STATES; i++) {
			totalevents += transitionMatrix(_current_note, i);
		}
		std::uniform_real_distribution<double> distributionNotesLenghts_(0, 1);
		targetSum = (distributionNotesLenghts_(generatorNotesSize_) * totalevents + 0.005);
		//do {
		auto x = transitionMatrix(_current_note, targetNote);
		while (targetNote < STATES &&
			sum + transitionMatrix(_current_note, targetNote) <= targetSum) {
			sum += transitionMatrix(_current_note, targetNote);
			targetNote++;
		}
		//	_current_note = (_current_note + 1) % STATES;
		//} while (targetNote < STATES && sum == 0);
		return targetNote;
	}

	/*!
	*  \brief Get a random value between 0 - 1
	*/
	double GetRandomValue() const {
		return (double)std::rand() / RAND_MAX;
	}
/*
	float CalculateFixDur(double testDur, double theorDur, float error) {
		if (fabs(testDur - theorDur) / theorDur <= error)
			return theorDur;
		else
			return 0;
	}	

	std::vector<std::vector<int>> GetTrainSetToFixedDurations(std::vector<std::vector<float>>& set) {
		std::vector<std::vector<int>> newSet;

		for (int i = 0; i < set.size(); ++i) {
			auto sizeNotes = set[i].size();
			std::vector<int> durs;
			for (int j = 0; j < set[i].size(); ++j) {
				float dur = 0.0f;
				double testDur = (set[i][j] * 1000.0);
				double errorDur = 0.15;
				int d;
				int indexDur = 0;
				for (d = 0; d < fixedDurations.size(); ++d) {
					auto tempDur = CalculateFixDur(set[i][j], fixedDurations[d], errorDur);
					if (tempDur >= fixedDurations[0]) {
						dur = tempDur;
						indexDur = d;
						break;
					}
				}
				if (d == fixedDurations.size())
					durs.push_back(d - 1);
				else
					durs.push_back(indexDur);
			}
			newSet.push_back(durs);
		}
		return newSet;
	}*/


};

#endif