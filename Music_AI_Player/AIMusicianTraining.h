#ifndef AIMusicianTraining_HEADER
#define AIMusicianTraining_HEADER

#include "KnowledgeRepresentation.h"

class  AIMusicianTraining {
public:
	//AIMusicianTraining();
	virtual KnowledgeRepresentation* Train() = 0;
	virtual KnowledgeRepresentation* Train(const KnowledgeRepresentation&) = 0;
};

#endif