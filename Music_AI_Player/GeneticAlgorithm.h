//
//  GeneticAlgorithm.h
//  music_ai
//
//  Created by Efrain Astudillo Vargas on 4/25/15.
//  Copyright (c) 2015 Efrain Astudillo. All rights reserved.
//

#ifndef music_ai_GeneticAlgorithm_h
#define music_ai_GeneticAlgorithm_h

#include <iostream>
#include "Initialization.h"
#include "Chromosome.h"
#include "MultiValueChromosome.h"
#include "Population.h"
#include "StopCriterias.h"
#include "IncrementalAlgorithm.h"

namespace  music {
  
  class FitnessNotes : public GaFitnessOperation {
  public:
    virtual float GACALL operator()( const GaChromosome* chromosome ) const

    {
      const vector<int>& chrom = dynamic_cast<const GaMultiValueChromosome<int> * >( chromosome ) ->GetCode();
      int score = 0;
      //  should implmeent Neural Network
    return score;
    }
    virtual GaParameters* GACALL MakeParameters() const { return NULL; }
    
    virtual bool GACALL CheckParameters(const GaParameters& parameters) const { return true; }
    
  };
  
  class FitnessDurations : public GaFitnessOperation {
  public:
	  virtual float GACALL operator()(const GaChromosome* chromosome) const
    {
    const vector<int>& chrom = dynamic_cast<const GaMultiValueChromosome<int> * >( chromosome ) ->GetCode();
    int score = 0;
    
    return score;
    }
    virtual GaParameters* GACALL MakeParameters() const { return NULL; }
    
    virtual bool GACALL CheckParameters(const GaParameters& parameters) const { return true; }
    
  };
  
  class ObserverNotes : public GaObserverAdapter {
  public:
	  virtual void GACALL NewBestChromosome(const GaChromosome& newChromosome, const GaAlgorithm& algorithm)
    {
    
    }
    virtual void GACALL EvolutionStateChanged(GaAlgorithmState newState, const GaAlgorithm& algorithm)
    {
      //if( newState == GAS_CRITERIA_STOPPED )
        
    }
  };
  
  class ObserverDurations : public GaObserverAdapter {
  public:
	  virtual void GACALL NewBestChromosome(const GaChromosome& newChromosome, const GaAlgorithm& algorithm)
    {
    
    }
    virtual void GACALL EvolutionStateChanged(GaAlgorithmState newState, const GaAlgorithm& algorithm)
    {
      //if( newState == GAS_CRITERIA_STOPPED )
    
    }
  };
  
  
  
  class AlgorithmGenetic {
  public:
    AlgorithmGenetic(){
      
      GaInitialize();
      
    }
  
    
    
  private:
    GaMultiValueSet<int> mValueSet;
   
  };

}
#endif
