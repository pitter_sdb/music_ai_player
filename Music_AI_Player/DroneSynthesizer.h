#ifndef DroneSynthesizer_HEADER
#define DroneSynthesizer_HEADER

#include "AISynthesisPerformer.h"

class  DroneSynthesizer : public AISynthesisPerformer {
public:
	DroneSynthesizer() : AISynthesisPerformer("drone", 0.2f){}
};

#endif