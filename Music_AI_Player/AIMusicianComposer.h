#ifndef AIMusicianComposer_HEADER
#define AIMusicianComposer_HEADER

#include "KnowledgeRepresentation.h"

#include "MidiUtilities.h"

using namespace MidiUtilities;

class  AIMusicianComposer{
public:	
  virtual ~AIMusicianComposer(){} //prevent memory leaks on derived classes
	
  virtual void Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set, 
		CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations) = 0;
	
  virtual CompositionOutput Compose() = 0;	
};

#endif