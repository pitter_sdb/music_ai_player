
#ifndef KnowledgeRepresentation_HEADER
#define KnowledgeRepresentation_HEADER

#include <iostream>
#include <vector>
#include "Eigen/Dense"
#include "MidiUtilities.h"

std::vector<float> fixedDurations{0.25, 0.5, 0.75, 1, 1.125, 1.5, 1.75, 2, 2.125, 2.5, 2.75, 3, 3.125, 3.5, 3.75, 4, 4.125, 4.5, 4.75 };

class  KnowledgeRepresentation {
//public:
//	KnowledgeRepresentation();
public:
	virtual ~KnowledgeRepresentation() {};
	virtual void Save() = 0;
	virtual void Load() = 0;
};


class CompositionOutput {
public:
	vector<int> notes;
	vector<float> durations;

	void GetMidiInfosFromNotes(std::vector<MidiUtilities::MidiInfo>& outputInfos) {
		for (size_t i = 0; i < notes.size(); i++) {
			outputInfos.push_back(MidiUtilities::MidiInfo(notes[i], 64, durations[i]));
		}
	}
};

namespace MusicOperation {

	enum MUSICAL_NOTE {C, 
		Csharp,
		D, 
		Dsharp,
		E,
		F,
		Fsharp,
		G,
		Gsharp,
		A,
		Asharp,
		B};

	vector<vector<int>> GetTrainSetToChromatic(vector<vector<int>>& set) {
		vector<vector<int>> newSet;
		for (int i = 0; i < set.size(); ++i) {
      //auto sizeNotes = set[i].size();
			vector<int> notes;
			for (int j = 0; j < set[i].size(); ++j) {
				int note = set[i][j] % 12;
				notes.push_back(note);
			}
			newSet.push_back(notes);
		}
		return newSet;
	};

	float CalculateFixDur(double testDur, double theorDur, float error) {
		if (fabs(testDur - theorDur) / theorDur <= error)
			return theorDur;
		else
			return -1;
	};

	std::vector<std::vector<int>> GetTrainSetToFixedDurations(std::vector<std::vector<float>>& set) {
		std::vector<std::vector<int>> newSet;

		for (int i = 0; i < set.size(); ++i) {
			auto sizeNotes = set[i].size();
			std::vector<int> durs;
			for (int j = 0; j < set[i].size(); ++j) {
				float dur = 0.0f;
				double testDur = (set[i][j] * 1000.0);
				//cout << "T: " << set[i][j] << endl;
				double errorDur = 0.15;
				int d;
				int indexDur = 0;
				for (d = 0; d < fixedDurations.size() - 1; ++d) {
					if (set[i][j] <= fixedDurations[0]) {
						indexDur = 0;
						break;
					}

					if (set[i][j] >= fixedDurations[fixedDurations.size() - 1]) {
						indexDur = fixedDurations.size() - 1;
						break;
					}
					if (set[i][j] >= fixedDurations[d] && set[i][j] <= fixedDurations[d + 1]) {
						auto diffLeft = set[i][j] - fixedDurations[d];
						auto diffRight = fixedDurations[d + 1] - set[i][j];

						if (diffLeft < diffRight)
							indexDur = d;
						else
							indexDur = d + 1;
						break;
					
					}
					/*auto tempDur = CalculateFixDur(set[i][j], fixedDurations[d], errorDur);
					if (tempDur >= fixedDurations[0]) {
						dur = tempDur;
						indexDur = d;
						break;
					} else {
						if (set[i][j] < fixedDurations[0]) {
							indexDur = 0;
							break;
						}
					}*/
				}
			/*	if (d == fixedDurations.size())
					indexDur = d - 1;*/
				durs.push_back(indexDur);
				//cout << "TF: " << fixedDurations[indexDur] << endl;
			}
			newSet.push_back(durs);
		}
		return newSet;
	};

}
#endif