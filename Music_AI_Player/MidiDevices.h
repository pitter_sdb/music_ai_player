#ifndef MidiDevices_HEADER
#define MidiDevices_HEADER

#include <vector>

#if defined(__WINDOWS_MM__)
#include <Windows.h>
#endif

#if defined(__MACOSX_CORE__)
#include <unistd.h>
#endif

#include <thread>
#include <functional>

#include "RtMidi.h"  
#include "SynthesisManager.h"
#include "MidiUtilities.h"

using namespace MidiUtilities;

namespace MidiDevices {

	struct MidiControl {
		int device;
		int code;
		int guiId;
		int value;

		MidiControl(int device_input, int code_input, int guiId_input, int value_input) {
			device = device_input;
			code = code_input;
			guiId = guiId_input;
			value = value_input;
		}

	};

	struct ControlLearning {
	public:

		bool readyToLearn;
		bool readyToForget;
		int guiId;
		void Learn(int gui_id) {
			guiId = gui_id;
			readyToLearn = true;
		};

		void Forget(int gui_id) {
			guiId = gui_id;
			readyToForget= true;
		};
	};

	bool startIsSet = false;
	unsigned long startTime_ = 0;
	std::vector<float> input_volumes_;
	std::vector<MidiControl> LearnedControls;
	ControlLearning controlLearning;

	int findInLearnedObjectsByDeviceControl(int device, int control) {
		for (size_t i = 0; i < LearnedControls.size(); i++) {
			if (LearnedControls[i].device == device && LearnedControls[i].code == control)
				return i;
		}
		return -1;
	}

	int findInLearnedObjectsByGuiId(int guiId) {
		for (size_t i = 0; i < LearnedControls.size(); i++) {
			if (LearnedControls[i].guiId == guiId)
				return i;
		}
		return -1;
	}

	void CheckMIDIControlMessage(int device, std::vector<unsigned char>& message) {
		int status = (int)message[0];
		int control = (int)message[1];
		int value = (int)message[2];
		bool areControls = status == 176;// Check this value, it is suppose to be midi controls in Ch 1
		if (areControls) {//It it is a control
			int learnedControlIndex = MidiDevices::findInLearnedObjectsByDeviceControl(device, control);
			bool controlIsInLearnedObjects = learnedControlIndex > 0;
			if (controlLearning.readyToLearn) {
				if (controlIsInLearnedObjects) {
					int learnedGuiControlIndex = MidiDevices::findInLearnedObjectsByGuiId(controlLearning.guiId);
					if (learnedGuiControlIndex >= 0)
						LearnedControls[learnedGuiControlIndex].guiId = -1; //forget last control
					LearnedControls[learnedControlIndex].guiId = controlLearning.guiId;
				}
				else {
					LearnedControls.push_back(MidiControl(device, control, controlLearning.guiId, value));
				}
				controlLearning.readyToLearn = false;
			}
			if (controlLearning.readyToForget) {
				int learnedGuiControlIndex = MidiDevices::findInLearnedObjectsByGuiId(controlLearning.guiId);
				if (learnedGuiControlIndex >= 0)
					LearnedControls[learnedGuiControlIndex].guiId = -1; //forget last control
				controlLearning.readyToForget = false;
			}
			if (controlIsInLearnedObjects) {
				LearnedControls[learnedControlIndex].value = value;
			}
		}
	}	

	MidiControl GetMidiControl(int guiId) {
		int learnedGuiControlIndex = findInLearnedObjectsByGuiId(controlLearning.guiId);
		if (learnedGuiControlIndex >= 0)
			return LearnedControls[learnedGuiControlIndex]; //forget last control
		else
			return MidiControl(-1, -1, -1, -1);
	}

	void FillMidiLine(RtMidiIn *midiin, int portN, std::vector<MidiInfo> &midiLine, bool &doneFlag, SynthesisManager& synthManager, std::vector<MidiMessageInTime>& midiMessages) {
		std::vector<unsigned char> message;
		float currentTime = 0.0f;
		startIsSet = false;
		
		// Periodically check input queue.
		std::cout << "Reading MIDI from port ... " << midiin->getPortName(portN) << " ... quit with Ctrl-C.\n" << std::endl;
		while (!doneFlag) {
			try {
				double stamp = midiin->getMessage(&message);				

				int nBytes = message.size();
				if (nBytes >= 3) {
					
					MidiDevices::CheckMIDIControlMessage(portN, message);

					int note = (int)message[1], velocity = (int)message[2];     
					int status = (int)message[0];
					bool areKeys = status == 144;// to avoid to take any other button that is not a key 90 //128 is 80
					if (!startIsSet) {
						startTime_ = clock();
						startIsSet = true;
					}
					if (velocity > 0 && areKeys) {				

						MidiInfo midiInfo(note, velocity);
						midiInfo.currentTime = currentTime;

						midiLine.push_back(midiInfo);
					} else {
						message[0] = 0x80;
						velocity = 0;
					}

					unsigned long clockCurrent = clock();
					currentTime = (clockCurrent - startTime_) / (CLOCKS_PER_SEC * 1.0f);
          //std::cout << "nota " << note << ", velocity " << velocity << std::endl;
					if(status == 144 || status == 128)
						synthManager.SendOSCMessage("/player", "/synthDefTest", portN, note, velocity, input_volumes_[portN]); //PLAYING NOTES

					//RECORDING SECTION

					/*MidiMessageInTime midiMessage;
					midiMessage.midiMessage = message;
					midiMessage.eventTime = currentTime;
					midiMessages.push_back(midiMessage);*/					
				}
			}
			catch (int e) {
				std::cout << "An exception occurred. Exception Nr. " << e << '\n';
			}

			// Sleep for 10 milliseconds ... platform-dependent.
#if defined(__WINDOWS_MM__)
			Sleep(10);
#elif defined(__MACOSX_CORE__)
      sleep(10); //
#endif
		}
	}	

	void GetMidiData(std::vector<MidiLine> &midiInfoSet, bool &doneFlag, SynthesisManager& synthManager,
		std::vector<std::vector<MidiMessageInTime>>& midiMessages) {

		//RtMidiIn midiin = new RtMidiIn();
		std::vector<RtMidiIn *> midiin_devices;
		doneFlag = false;
		RtMidiIn *firstMidiDevice = new RtMidiIn(RtMidi::UNSPECIFIED, (std::string("RtMidiInputClient")) , 10000);
		// Check available ports.
		unsigned int nPorts = firstMidiDevice->getPortCount();
		if (nPorts == 0) {
			std::cout << "No ports available!\n";
		}
		else {
			std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";

			std::string portName;
			midiInfoSet.resize(nPorts);
			midiMessages.resize(nPorts);
			//Adding more midiIn controllers
			midiin_devices.push_back(firstMidiDevice);
			for (unsigned int i = 1; i < nPorts; ++i)
				midiin_devices.push_back(new RtMidiIn(RtMidi::UNSPECIFIED, (std::string("RtMidiInputClient")), 10000));

			//Initizalizing controllers
			for (unsigned int i = 0; i < nPorts; ++i) {
				try {
					auto midiin = midiin_devices[i];
					portName = midiin->getPortName(i);
					std::cout << "  Input Port #" << i + 1 << ": " << portName << '\n';
					midiin->openPort(i);
					// Don't ignore sysex, timing, or active sensing messages.
					midiin->ignoreTypes(false, false, false);
					input_volumes_.push_back(0.5f);//default volume for each port
				}
				catch (RtMidiError &error) {
					error.printMessage();
					goto cleanup;
				}
			}

			//Executing threads

			std::vector<std::thread> threadsMidiDevices;

			//std::vector<std::vector<MidiMessageInTime>> midiMessagesForThreads(midiin_devices.size());
			for (std::size_t i = 0; i < midiin_devices.size(); ++i)
        threadsMidiDevices.push_back(std::thread(&FillMidiLine,
				std::ref(midiin_devices[i]),
				i,
				std::ref(midiInfoSet[i]),
				std::ref(doneFlag),
				std::ref( synthManager),
				std::ref(midiMessages[i])));

      for (auto& th : threadsMidiDevices) th.join();
			/*for (auto& msgArrays : midiMessagesForThreads) {
				for (size_t i = 0; i < msgArrays.size(); i++) {
					midiMessages.push_back(msgArrays[i]);
				}
			}*/
		}

	cleanup:
		for (std::size_t i = 0; i < midiin_devices.size(); ++i)
			delete midiin_devices[i];
		/*delete midiOut;*/

	}
}

#endif