#ifndef ComposerComposite_HEADER
#define ComposerComposite_HEADER

#include "OscOutboundPacketStream.h"
#include "UdpSocket.h"
#include <vector>
#include "MusicImproviser.h"
#include <iostream>
#include <random>
#include <numeric>
#include <algorithm>
#include "SynthesisManager.h"
#include "AIMusicianComposer.h"

class  ComposerComposite : public AIMusicianComposer {
public:
	//RandomComposer(){}
	void Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set,
		CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations);
	CompositionOutput Compose() {
		CompositionOutput o;
		return o;
	}

	void Add(AIMusicianComposer* composer) {
		composers_.push_back(composer);
	}

	~ComposerComposite() {
		for (auto comp : composers_) delete comp;
	}

private:
	std::vector<AIMusicianComposer*> composers_;
};

void ComposerComposite::Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set,
	CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations) {

	std::vector<MidiInfo> new_input_set(input_set);
	for (size_t i = 0; i < composers_.size(); i++) {
		composers_[i]->Apply(keyNote, new_input_set, output, globalNotes, globalDurations);		
		if (i < composers_.size() - 1) {
			output.GetMidiInfosFromNotes(new_input_set);
			output.notes.clear();
			output.durations.clear();
		}
	}
	globalNotes.insert(globalNotes.end(), output.notes.begin(), output.notes.end());
	globalDurations.insert(globalDurations.end(), output.durations.begin(), output.durations.end());
}



#endif