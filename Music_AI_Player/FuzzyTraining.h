#ifndef FuzzyTraining_HEADER
#define FuzzyTraining_HEADER

#include "AIMusicianTraining.h"
#include "KnowledgeRepresentation.h"
#include "FuzzyKnowledgeRepresentation.h"
#include <iostream>
#include <stdlib.h>
#include <algorithm> 
#include "BasicMidiKnowledgeRepresentation.h"

class FuzzyTraining : public  AIMusicianTraining {
public:
	FuzzyTraining(FuzzyKnowledgeRepresentation* startingKw, std::vector<std::vector<int>>& setNotesToTrain, std::vector<std::vector<float>>& setDursToTrain, int trainSetSize) :
		humanNotes_(setNotesToTrain), humanDurations_(setDursToTrain)
	{
		knowledge_ = startingKw;
		trainSetSize_ = trainSetSize;
	};

	FuzzyTraining(FuzzyKnowledgeRepresentation* startingKw) {
		knowledge_ = startingKw;
	};

	KnowledgeRepresentation* Train();
	KnowledgeRepresentation* Train(const KnowledgeRepresentation&);
private:
	std::vector<std::vector<int>> humanNotes_;
	std::vector<std::vector<float>> humanDurations_;
	FuzzyKnowledgeRepresentation * knowledge_;
	int trainSetSize_;
};

KnowledgeRepresentation* FuzzyTraining::Train() {
	for (size_t i = 0; i < trainSetSize_; i++) {
		FuzzyKnowledgeRepresentation::FuzzyMusicMaterial fuzzyMat;
		std::cout << "RECORD "<< i + 1 << std::endl << "Enter KeyNote: ";
		std::cin >> fuzzyMat.keyNote;
		std::cout << "Enter Tempo: ";
		std::cin >> fuzzyMat.tempo;
		FuzzyKnowledgeRepresentation::FuzzyEmotionVar emotiVar;
		std::cout << "Enter Level of Alegria: ";
		emotiVar.emotion = (FuzzyKnowledgeRepresentation::Emotion)0;
		std::cin >> emotiVar.degree;
		fuzzyMat.emotionsSet.push_back(emotiVar);
		std::cout << "Enter Level of Serenidad: ";
		emotiVar.emotion = (FuzzyKnowledgeRepresentation::Emotion)1;
		std::cin >> emotiVar.degree;
		fuzzyMat.emotionsSet.push_back(emotiVar);
		std::cout << "Enter Level of Tristeza: ";
		emotiVar.emotion = (FuzzyKnowledgeRepresentation::Emotion)2;
		std::cin >> emotiVar.degree;
		fuzzyMat.emotionsSet.push_back(emotiVar);
		std::cout << "Enter Level of Nostalgia: ";
		emotiVar.emotion = (FuzzyKnowledgeRepresentation::Emotion)3;
		std::cin >> emotiVar.degree;
		fuzzyMat.emotionsSet.push_back(emotiVar);
		std::cout << "Enter Level of Apasionamiento: ";
		emotiVar.emotion = (FuzzyKnowledgeRepresentation::Emotion)4;
		std::cin >> emotiVar.degree;
		fuzzyMat.emotionsSet.push_back(emotiVar);

		for (size_t j = 0; j < humanNotes_[i].size(); j++) {
			fuzzyMat.melodyPattern.push_back(MidiUtilities::MidiInfo(humanNotes_[i][j], humanDurations_[i][j]));
		}
		knowledge_->givenPatterns.push_back(fuzzyMat);
		knowledge_->allPatterns.push_back(fuzzyMat);
		knowledge_->Save();
	}	
	return knowledge_;
}

KnowledgeRepresentation* FuzzyTraining::Train(const KnowledgeRepresentation& info) {
	const BasicMidiKnowledgeRepresentation* roughKnowledge = static_cast<const BasicMidiKnowledgeRepresentation*>(&info);



	/////// Calculate Distances ///////////// 
	vector<FuzzyKnowledgeRepresentation::FuzzyMusicMaterial> newKnowledgeFuzzyMatNotes;
	vector<FuzzyKnowledgeRepresentation::FuzzyMusicMaterial> newKnowledgeFuzzyMatDurations;

	for (size_t n = 0; n < roughKnowledge->midiInfoSequences.size(); ++n) {
		float minPatternDeltaNote = 12;
		float minPatternDeltaDuration = 4;
		FuzzyKnowledgeRepresentation::FuzzyMusicMaterial* minFuzzyMaterial_notes; // it has teh emotion set
		FuzzyKnowledgeRepresentation::FuzzyMusicMaterial* minFuzzyMaterial_durations; // it has teh emotion set

		auto patternsFromSavedKnowledge = &knowledge_->givenPatterns;
		//NOTES approach * CONSIDERS KEY NOTE, oR IT will not wor
		for (size_t m = 0; m < patternsFromSavedKnowledge->size(); ++m) {

			auto fuzzyMaterialSavedPattern = &(*patternsFromSavedKnowledge)[m];
			auto savedPattern = &fuzzyMaterialSavedPattern->melodyPattern;
			auto newPattern = &roughKnowledge->midiInfoSequences[n];

			int savedPatterSize = savedPattern->size();
			int newPatterSize = newPattern->size();

			int minPattternSize = (std::min)(savedPatterSize, newPatterSize);

			int Nsegments = 1 + abs(savedPatterSize - newPatterSize);
			
			float minSegmentDeltaNotes = 12;
			float minSegmentDeltaDurations = 4;

			for (size_t i = 0; i < Nsegments; ++i) {

				//NOTES Process
				int sumDeltaNotes = 0;
				for (size_t j = 0; j < minPattternSize; ++j) {
					int indexSavedPatt, indexNewPatt;
					if (savedPatterSize >= newPatterSize) {
						indexSavedPatt = j + i;
						indexNewPatt = j;
					} else {
						indexSavedPatt = j;
						indexNewPatt = j + i;
					}

					int savePatternNote = (*savedPattern)[indexSavedPatt].note - fuzzyMaterialSavedPattern->keyNote; // REFERENCE KEY NOTE
					int newPatterNote = (*newPattern)[indexNewPatt].note; // asume it is C
					sumDeltaNotes += abs(savePatternNote - newPatterNote) % 12; // DELTAS
				}
				float avgSegmentDeltaNotes = 1.0f * sumDeltaNotes / (float)minPattternSize;
				if (avgSegmentDeltaNotes < minSegmentDeltaNotes) {
					minSegmentDeltaNotes = avgSegmentDeltaNotes;
				}	

				//DURATION Process

				int sumDeltaDurations = 0;
				for (size_t j = 0; j < minPattternSize; ++j) {
					int indexSavedPatt, indexNewPatt;
					if (savedPatterSize >= newPatterSize) {
						indexSavedPatt = j + i;
						indexNewPatt = j;
					} else {
						indexSavedPatt = j;
						indexNewPatt = j + i;
					}

					int savePatternDuration = (*savedPattern)[indexSavedPatt].duration;
					int newPatterDuration = (*newPattern)[indexNewPatt].duration; 
					sumDeltaDurations += (std::min)(abs(savePatternDuration - newPatterDuration), 4); // DELTAS durations
				}
				float avgSegmentDeltaDurations = 1.0f * sumDeltaDurations / (float)minPattternSize;
				if (avgSegmentDeltaDurations < minSegmentDeltaDurations) {
					minSegmentDeltaDurations = avgSegmentDeltaDurations;
				}


			}


			//NOTE min calculation
			float patternDeltaNote = minSegmentDeltaNotes;

			if (patternDeltaNote < minPatternDeltaNote) {
				minPatternDeltaNote = patternDeltaNote;
				minFuzzyMaterial_notes = fuzzyMaterialSavedPattern;
			}


			//DURATION min calculation

			float patternDeltaDuration = minSegmentDeltaDurations;

			if (patternDeltaDuration < minPatternDeltaDuration) {
				minPatternDeltaDuration = patternDeltaDuration;
				minFuzzyMaterial_durations = fuzzyMaterialSavedPattern;
			}
		}

		FuzzyKnowledgeRepresentation::FuzzyMusicMaterial newFuzzyMusicMaterialNotes;
		FuzzyKnowledgeRepresentation::FuzzyMusicMaterial newFuzzyMusicMaterialDurations;
		newFuzzyMusicMaterialNotes.keyNote = newFuzzyMusicMaterialDurations.keyNote = 0;
		newFuzzyMusicMaterialNotes.tempo = newFuzzyMusicMaterialDurations.tempo = 60.0; // relative to 60 BPM in order to keep the referencial durations
		for (size_t e = 0; e < minFuzzyMaterial_notes->emotionsSet.size(); e++) {
			FuzzyKnowledgeRepresentation::FuzzyEmotionVar emotionVar;
			
			//NOTES
			emotionVar = minFuzzyMaterial_notes->emotionsSet[e];
			emotionVar.degree = emotionVar.degree * (1 - minPatternDeltaNote / 11.0f); // NORMALIZATION
			newFuzzyMusicMaterialNotes.emotionsSet.push_back(emotionVar);

			//DURATIONS
			emotionVar = minFuzzyMaterial_durations->emotionsSet[e];
			emotionVar.degree = emotionVar.degree * (1 - minPatternDeltaDuration / 4.0f); // NORMALIZATION
			newFuzzyMusicMaterialDurations.emotionsSet.push_back(emotionVar);

		}


		//NOTES Add to melody pattern
		newFuzzyMusicMaterialNotes.melodyPattern = roughKnowledge->midiInfoSequences[n];
		newKnowledgeFuzzyMatNotes.push_back(newFuzzyMusicMaterialNotes);

		//DURATION Add to melody pattern
		newFuzzyMusicMaterialDurations.melodyPattern = roughKnowledge->midiInfoSequences[n];
		newKnowledgeFuzzyMatDurations.push_back(newFuzzyMusicMaterialDurations);

	}

	// ALL MATERIAL

	vector<FuzzyKnowledgeRepresentation::FuzzyMusicMaterial> allNewMaterial;

	knowledge_->allPatterns.clear();

	allNewMaterial.reserve(knowledge_->givenPatterns.size() + newKnowledgeFuzzyMatNotes.size()); // preallocate memory



	allNewMaterial.insert(allNewMaterial.end(), knowledge_->givenPatterns.begin(), knowledge_->givenPatterns.end());
	allNewMaterial.insert(allNewMaterial.end(), newKnowledgeFuzzyMatNotes.begin(), newKnowledgeFuzzyMatNotes.end());
	allNewMaterial.insert(allNewMaterial.end(), newKnowledgeFuzzyMatDurations.begin(), newKnowledgeFuzzyMatDurations.end());

	knowledge_->allPatterns = allNewMaterial;


	// NOTES

	vector<FuzzyKnowledgeRepresentation::FuzzyMusicMaterial> generatedNewMaterialNotes;

	knowledge_->generatedPatternsNotes.clear();

	generatedNewMaterialNotes.reserve(knowledge_->generatedPatternsNotes.size() + newKnowledgeFuzzyMatNotes.size()); // preallocate memory
	generatedNewMaterialNotes.insert(generatedNewMaterialNotes.end(), knowledge_->generatedPatternsNotes.begin(), knowledge_->generatedPatternsNotes.end());
	generatedNewMaterialNotes.insert(generatedNewMaterialNotes.end(), newKnowledgeFuzzyMatNotes.begin(), newKnowledgeFuzzyMatNotes.end());

	knowledge_->generatedPatternsNotes = generatedNewMaterialNotes;


	// DURATIONS

	vector<FuzzyKnowledgeRepresentation::FuzzyMusicMaterial> generatedNewMaterialDurations;

	knowledge_->generatedPatternsDurations.clear();

	generatedNewMaterialDurations.reserve(knowledge_->generatedPatternsDurations.size() + newKnowledgeFuzzyMatDurations.size()); // preallocate memory
	generatedNewMaterialDurations.insert(generatedNewMaterialDurations.end(), knowledge_->generatedPatternsDurations.begin(), knowledge_->generatedPatternsDurations.end());
	generatedNewMaterialDurations.insert(generatedNewMaterialDurations.end(), newKnowledgeFuzzyMatDurations.begin(), newKnowledgeFuzzyMatDurations.end());

	knowledge_->generatedPatternsDurations = generatedNewMaterialDurations;

	//FILL TREES

	knowledge_->FillBinaryTrees();

	return knowledge_;
}

#endif