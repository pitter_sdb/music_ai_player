#ifndef AISynthesisPerformer_HEADER
#define AISynthesisPerformer_HEADER

#include<string>

class  AISynthesisPerformer {
protected:
	AISynthesisPerformer(const std::string& name, float volume) : name_(name) {
		volume_ = volume;
		}

public:
  const std::string& GetName() const{
		return name_;
	}

	void SetName(const std::string& name){
		name_ = name;
	}

	float GetVolume() const{
		return volume_;
	}

	void SetVolume(float volume) {
		volume_ = volume;
	}
private:
	std::string name_;
	float volume_;
};

#endif