#ifndef TriSynthesizer_HEADER
#define TriSynthesizer_HEADER

#include "AISynthesisPerformer.h"

class  TriSynthesizer : public AISynthesisPerformer {
public:
	TriSynthesizer() : AISynthesisPerformer("synthe2", 0.5f) {}
};

#endif
