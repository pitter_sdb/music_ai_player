#ifndef MarkovKnowledgeRepresentation_HEADER
#define MarkovKnowledgeRepresentation_HEADER

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include "Eigen/Dense"
#include "KnowledgeRepresentation.h"

class  MarkovKnowledgeRepresentation : public  KnowledgeRepresentation {
public:
	Eigen::MatrixXd TransitionMatrixNotes;
	Eigen::MatrixXd TransitionMatrixDurations;

	void Save() {
		WriteInFile("notesMatrixTransition.csv", ";", TransitionMatrixNotes);
		WriteInFile("durationsMatrixTransition.csv", ";", TransitionMatrixDurations);
	}

	void Load() {
		LoadFromFile("notesMatrixTransition.csv", ";", TransitionMatrixNotes);
		LoadFromFile("durationsMatrixTransition.csv", ";", TransitionMatrixDurations);
	}
private:
	void WriteInFile(std::string fileName, std::string separator, const Eigen::MatrixXd& matrix);
	void LoadFromFile(std::string fileName, std::string separator, Eigen::MatrixXd& matrix);
};


void MarkovKnowledgeRepresentation::WriteInFile(std::string fileName, std::string separator, const Eigen::MatrixXd& matrix) {
	std::ofstream matrixFile;
	matrixFile.open(fileName);

	for (int i = 0; i < matrix.rows(); ++i) {
		for (int j = 0; j < matrix.cols(); ++j) {
			matrixFile << matrix(i, j);
			if (j != matrix.cols() - 1)
				matrixFile << separator;
		}
		matrixFile << "\n";
	}

	matrixFile.close();
};

void MarkovKnowledgeRepresentation::LoadFromFile(std::string fileName, std::string separator, Eigen::MatrixXd& matrix) {
	//http://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
	std::vector<std::vector<float>> vectorMatrix;
	std::string line;
	std::ifstream myfile(fileName);
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			std::vector<float> elementProbabilities;
			std::string s = line;
			size_t pos = 0;
			std::string token;
			while ((pos = s.find(separator)) != std::string::npos) {
				token = s.substr(0, pos);
				elementProbabilities.push_back(std::stof(token));
				s.erase(0, pos + separator.length());
			}
			elementProbabilities.push_back(std::stof(s));
			vectorMatrix.push_back(elementProbabilities);
		}
		myfile.close();
	}

	auto  matrixSize = vectorMatrix.size();

	matrix = Eigen::MatrixXd::Zero(matrixSize, matrixSize);

	for (size_t i = 0; i < matrixSize; i++){
		for (size_t j = 0; j < matrixSize; j++)	{
			matrix(i, j) = vectorMatrix[i][j];
		}
	}

};

#endif