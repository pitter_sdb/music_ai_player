#ifndef MidiUtilities_HEADER
#define MidiUtilities_HEADER

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include "midifile/MidiFile.h"
#include "midifile/Options.h"

namespace MidiUtilities {

	struct MidiInfo {
		int note;
		int velocity;
		float currentTime;
		float duration;

		MidiInfo() {};
		MidiInfo(int n, int v) :note(n), velocity(v) {}
		MidiInfo(int n, float dur) :note(n), duration(dur) {}
		MidiInfo(int n, int v, float dur) :note(n), velocity(v), duration(dur) {}
	};

	struct MidiMessageInTime {
		std::vector<unsigned char> midiMessage;
		float eventTime;
	};

	using MidiLine = std::vector < MidiInfo >;

	float GetSecondsPerQuarterNote(MidiFile& midifile) {
		string bmp_str;

		std::stringstream stream;

		bmp_str = "";
		string param;
		int indexTempo = 0;

		for (int i = 0; i <= midifile[0].size(); ++i) {
			if (midifile[0][i][1] == 0x51 && midifile[0][i][2] == 0x03) {
				indexTempo = i;
				break;
			}
		}

		for (int i = 3; i <= 5; ++i) {
			stream << hex << (int)midifile[0][indexTempo][i];
			stream >> param;
			if (param.size() < 2)
				param = "0" + param;
			bmp_str += param;
			stream.clear();
		}

		bmp_str = "0x" + bmp_str;
		return std::stoul(bmp_str, nullptr, 16) / 1000000.0f;
	}

	std::vector<uchar> BMPtoMidiEvent(int BMP) {
		std::vector<uchar> midieventHeader{ 0xff, 0x51, 0x03 };
		int tempoMidi = (int)((60.0 / BMP) * 1000000.0);
		string bmp_str;
		std::stringstream stream;

		stream << hex << tempoMidi;
		stream >> bmp_str;
		if (bmp_str.size() < 6)
			bmp_str = "0" + bmp_str;

		string param;
		param = "0x";
		param.push_back(bmp_str[0]);
		param.push_back(bmp_str[1]);
		midieventHeader.push_back(std::stoul(param, nullptr, 16));

		param = "0x";
		param.push_back(bmp_str[2]);
		param.push_back(bmp_str[3]);
		midieventHeader.push_back(std::stoul(param, nullptr, 16));

		param = "0x";
		param.push_back(bmp_str[4]);
		param.push_back(bmp_str[5]);
		midieventHeader.push_back(std::stoul(param, nullptr, 16));

		return midieventHeader;
	}

	int WriteMidiFile(std::string fileName, int track, int tpq, int BPM, std::vector<int>& notes, std::vector<float>& durations) {
		MidiFile outputfile;        // create an empty MIDI file with one track
		outputfile.absoluteTime();  // time information stored as absolute time
		// (will be coverted to delta time when written)
		outputfile.addTrack(1);     // Add another two tracks to the MIDI file
		vector<uchar> midievent;     // temporary storage for MIDI events
		midievent.resize(3);        // set the size of the array to 3 bytes
		//int tpq = 200;              // default value in MIDI file is 48
		outputfile.setTicksPerQuarterNote(tpq);

		//// data to write to MIDI file: (60 = middle C)
		//// C5 C  G G A A G-  F F  E  E  D D C-
		//int melody[50] = { 72, 72, 79, 79, 81, 81, 79, 77, 77, 76, 76, 74, 74, 72, -1 };
		//int mrhythm[50] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, -1 };

		//// C3 C4 E C F C E C D B3 C4 A3 F G C-
		//int bass[50] = { 48, 60, 64, 60, 65, 60, 64, 60, 62, 59, 60, 57, 53, 55, 48, -1 };
		//int brhythm[50] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, -1 };


		// store a melody line in track 1 (track 0 left empty for conductor info)

		//FF 51 03 tt tt tt // This is for tempo
		//493E0 // 200BMP

		notes.push_back(-1);
		durations.push_back(-1);
		std::vector<uchar> _temp = BMPtoMidiEvent(BPM);
		outputfile.addEvent(0, 0, BMPtoMidiEvent(BPM));

		int i = 0;
		int actiontime = 0;      // temporary storage for MIDI event time
		midievent[2] = 64;       // store attack/release velocity for note command
		while (notes.size() > 0 && notes[i] >= 0) {
			midievent[0] = 0x90;     // store a note on command (MIDI channel 1)
			midievent[1] = notes[i];
			outputfile.addEvent(1, actiontime, midievent);
			actiontime += (int)(tpq * durations[i] * BPM / 60); 
			midievent[0] = 0x80;     // store a note on command (MIDI channel 1)
			outputfile.addEvent(1, actiontime, midievent);
			i++;
		}

		outputfile.sortTracks();         // make sure data is in correct order
		//outputfile.write("outputComputer.mid"); // write Standard MIDI File twinkle.mid
		outputfile.write(fileName); // write Standard MIDI File twinkle.mid
		return 0;
	}

	int WriteMidiFile(std::string fileName, int track, int tpq, int BPM, std::vector<MidiMessageInTime>& midiMessages) {
		MidiFile outputfile;        // create an empty MIDI file with one track
		outputfile.absoluteTime();  // time information stored as absolute time
		// (will be coverted to delta time when written)
		outputfile.addTrack(1);     // Add another two tracks to the MIDI file
		vector<uchar> midievent;     // temporary storage for MIDI events
		midievent.resize(3);        // set the size of the array to 3 bytes
		//int tpq = 200;              // default value in MIDI file is 48
		outputfile.setTicksPerQuarterNote(tpq);

		//// data to write to MIDI file: (60 = middle C)
		//// C5 C  G G A A G-  F F  E  E  D D C-
		//int melody[50] = { 72, 72, 79, 79, 81, 81, 79, 77, 77, 76, 76, 74, 74, 72, -1 };
		//int mrhythm[50] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, -1 };

		//// C3 C4 E C F C E C D B3 C4 A3 F G C-
		//int bass[50] = { 48, 60, 64, 60, 65, 60, 64, 60, 62, 59, 60, 57, 53, 55, 48, -1 };
		//int brhythm[50] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, -1 };


		// store a melody line in track 1 (track 0 left empty for conductor info)

		//FF 51 03 tt tt tt // This is for tempo
		//493E0 // 200BMP


		std::vector<uchar> _temp = BMPtoMidiEvent(BPM);
		outputfile.addEvent(0, 0, BMPtoMidiEvent(BPM));

		int i = 0;
		int actiontime = 0;      // temporary storage for MIDI event time
		midievent[2] = 64;       // store attack/release velocity for note command

		for (size_t i = 0; i < midiMessages.size(); ++i) {
				midievent[0] = midiMessages[i].midiMessage[0];     // store a note on command (MIDI channel 1)
				midievent[1] = midiMessages[i].midiMessage[1];
				midievent[2] = midiMessages[i].midiMessage[2];
				actiontime = (int)(tpq * midiMessages[i].eventTime * BPM / 60 );//(int)(tpq * durations[i] * BPM / 120);
				outputfile.addEvent(1, actiontime, midievent);			
		}

		midievent[0] = 144;     // store a note on command (MIDI channel 1)
		midievent[1] = 0;
		midievent[2] = 0;
		actiontime += (int)(tpq * BPM / 60);
		outputfile.addEvent(1, actiontime, midievent);
		
		outputfile.sortTracks();         // make sure data is in correct order
		//outputfile.write("outputComputer.mid"); // write Standard MIDI File twinkle.mid
		outputfile.write(fileName); // write Standard MIDI File twinkle.mid
		return 0;
	}

	int WriteMidiFileAllTracks(std::string fileName, int tpq, int BPM, std::vector<std::vector<MidiMessageInTime>>& midiMessagesTracks) {
		MidiFile outputfile;        // create an empty MIDI file with one track
		outputfile.absoluteTime();  // time information stored as absolute time
		// (will be coverted to delta time when written)
		outputfile.addTrack(midiMessagesTracks.size() + 1);     // Add another two tracks to the MIDI file
		vector<uchar> midievent;     // temporary storage for MIDI events
		midievent.resize(3);        // set the size of the array to 3 bytes
		//int tpq = 200;              // default value in MIDI file is 48
		outputfile.setTicksPerQuarterNote(tpq);

		//// data to write to MIDI file: (60 = middle C)
		//// C5 C  G G A A G-  F F  E  E  D D C-
		//int melody[50] = { 72, 72, 79, 79, 81, 81, 79, 77, 77, 76, 76, 74, 74, 72, -1 };
		//int mrhythm[50] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, -1 };

		//// C3 C4 E C F C E C D B3 C4 A3 F G C-
		//int bass[50] = { 48, 60, 64, 60, 65, 60, 64, 60, 62, 59, 60, 57, 53, 55, 48, -1 };
		//int brhythm[50] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, -1 };


		// store a melody line in track 1 (track 0 left empty for conductor info)

		//FF 51 03 tt tt tt // This is for tempo
		//493E0 // 200BMP


		std::vector<uchar> _temp = BMPtoMidiEvent(BPM);
		outputfile.addEvent(0, 0, BMPtoMidiEvent(BPM));

		

		for (size_t track = 1; track <= midiMessagesTracks.size(); ++track) {
			auto midiMessages = midiMessagesTracks[track - 1];

			int i = 0;
			int actiontime = 0;      // temporary storage for MIDI event time
			midievent[2] = 64;       // store attack/release velocity for note command

			for (size_t i = 0; i < midiMessages.size(); ++i) {
				midievent[0] = midiMessages[i].midiMessage[0];     // store a note on command (MIDI channel 1)
				midievent[1] = midiMessages[i].midiMessage[1];
				midievent[2] = midiMessages[i].midiMessage[2];
				actiontime = (int)(tpq * midiMessages[i].eventTime * BPM / 60);//(int)(tpq * durations[i] * BPM / 120);				
				outputfile.addEvent(track, actiontime, midievent);
			}

			midievent[0] = 144;     // store a note on command (MIDI channel 1)
			midievent[1] = 0;
			midievent[2] = 0;
			actiontime += (int)(tpq * BPM / 60);
			outputfile.addEvent(track, actiontime, midievent);
		}


		outputfile.sortTracks();         // make sure data is in correct order
		//outputfile.write("outputComputer.mid"); // write Standard MIDI File twinkle.mid
		outputfile.write(fileName); // write Standard MIDI File twinkle.mid
		return 0;
	}

	void ShowMidiFileInformation(MidiFile& midifile) {

		float secondsPerQuarterNote = 0.2f;//GetSecondsPerQuarterNote(midifile);

		cout << "BMP: " << (1.0f / secondsPerQuarterNote) * 60 << endl;
		//midifile.deltaTime();
		int tracks = midifile.getTrackCount();

		cout << "TPQ: " << midifile.getTicksPerQuarterNote() << endl;
		if (tracks > 1) {
			cout << "TRACKS: " << tracks << endl;
		}
		for (int track = 0; track < tracks; track++) {
			if (tracks > 1) {
				cout << "\nTrack " << track << endl;
			}
			for (int event = 0; event < midifile[track].size(); event++) {
				cout << dec << midifile[track][event].tick << '\t';
				cout << dec << secondsPerQuarterNote * midifile[track][event].tick / midifile.getTicksPerQuarterNote();
				cout << '\t' << hex;
				for (int i = 0; i < midifile[track][event].size(); i++) {
					cout << (int)midifile[track][event][i] << ' ';
				}
				cout << endl;
			}
		}
	}
}

#endif














//---------------------------------------- CODE TO REVIEW LATER ---------------------------

//// midiprobe.cpp
//#include <iostream>
//#include <cstdlib>
//
//int main() 
//{
//	RtMidiIn  *midiin = 0; 
//	RtMidiOut *midiout = 0;
//	// RtMidiIn constructor
//	try {
//		midiin = new RtMidiIn();
//		std::cout << "OK" << std::endl;
//	}
//	catch (RtMidiError &error) {
//		error.printMessage();
//		exit(EXIT_FAILURE);
//	}
//	// Check inputs.
//	unsigned int nPorts = midiin->getPortCount();
//	std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";
//	std::string portName;
//	for (unsigned int i = 0; i<nPorts; i++) {
//		try {
//			portName = midiin->getPortName(i);
//		}
//		catch (RtMidiError &error) {
//			error.printMessage();
//			goto cleanup;
//		}
//		std::cout << "  Input Port #" << i + 1 << ": " << portName << '\n';
//	}
//	// RtMidiOut constructor
//	try {
//		midiout = new RtMidiOut();
//	}
//	catch (RtMidiError &error) {
//		error.printMessage();
//		exit(EXIT_FAILURE);
//	}
//	// Check outputs.
//	nPorts = midiout->getPortCount();
//	std::cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
//	for (unsigned int i = 0; i<nPorts; i++) {
//		try {
//			portName = midiout->getPortName(i);
//		}
//		catch (RtMidiError &error) {
//			error.printMessage();
//			goto cleanup;
//		}
//		std::cout << "  Output Port #" << i + 1 << ": " << portName << '\n';
//	}
//	std::cout << '\n';
//	// Clean up
//cleanup:
//	delete midiin;
//	delete midiout;
//		
//	std::cin.get();
//	return 0;
//}


//qmidiin.cpp

//
//#include <thread> 
//
//#include <windows.h> 
//#include <iostream>
//#include <cstdlib>
//#include <signal.h>
//#include "RtMidi.h"
//
//#include "Metronome.h"
//
//bool done;
//static void finish(int ignore){ done = true; }
//
//void OnBeatPerform(){
//	std::cout << "OK" << std::endl;
//}
//
//Metronome m(80, &OnBeatPerform);
//
//void UpdateMetronome(){
//	m.Update();
//}
//
//
//int main()
//{
//	int limit = 10;
//	int counter = 0;
//
//	Metronome m(80);
//	m.Start();
//
//	std::thread metronomeThread(UpdateMetronome);
//	std::cout << "After" << std::endl;
//	/*while (m.IsPlaying()){
//		;
//	}*/
//	/*while (counter < limit){
//		++counter;
//		Sleep(1000);
//	}
//
//	m.Finish();*/
//	metronomeThread.join();
//	m.Finish();
//	std::cout << "FInish" << std::endl;
//	std::getchar();
//
//
//
////
////
////
////
////	RtMidiIn *midiin = new RtMidiIn();
////	std::vector<unsigned char> message;
////	int nBytes, i;
////	double stamp;
////	// Check available ports.
////	unsigned int nPorts = midiin->getPortCount();
////	if (nPorts == 0) {
////		std::cout << "No ports available!\n";
////		goto cleanup;
////	}
////	midiin->openPort(0);
////	// Don't ignore sysex, timing, or active sensing messages.
////	midiin->ignoreTypes(false, false, false);
////	// Install an interrupt handler function.
////	done = false;
////	(void)signal(SIGINT, finish);
////	// Periodically check input queue.
////	std::cout << "Reading MIDI from port ... quit with Ctrl-C.\n";
////	while (!done) {
////		stamp = midiin->getMessage(&message);
////		nBytes = message.size();
////		if (nBytes > 1)
////			for (i = 0; i < nBytes; i++){
////			std::cout << "Byte " << i << " = " << (int)message[i] << ", ";
////			//if (nBytes > 0)
////			std::cout << "stamp = " << stamp << std::endl;
////			}
////		// Sleep for 10 milliseconds ... platform-dependent.
////		Sleep(10);
////	}
////	// Clean up
////cleanup:
////	delete midiin;
////	return 0;
//}