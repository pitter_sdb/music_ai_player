#ifndef GeneticTraining_HEADER
#define GeneticTraining_HEADER

#include "AIMusicianTraining.h"
#include "KnowledgeRepresentation.h"
#include <iostream>
#include <vector>

class GeneticTraining : public  AIMusicianTraining {
public:
  GeneticTraining(std::vector<std::vector<int>>& setNotesToTrain, std::vector<std::vector<float>>& setDursToTrain) :
		chromaticNotesSet_(MusicOperation::GetTrainSetToChromatic(setNotesToTrain)),
		fixedDurationsSet_(MusicOperation::GetTrainSetToFixedDurations(setDursToTrain)) {
      markovKnowledge_.TransitionMatrixNotes = Eigen::MatrixXd::Zero(notesSize_, notesSize_);
      markovKnowledge_.TransitionMatrixDurations = Eigen::MatrixXd::Zero(durationsSize_, durationsSize_);
    }

	KnowledgeRepresentation* Train();
	KnowledgeRepresentation* Train(const KnowledgeRepresentation&);
  
private:
  int notesSize_ = 12;
  int durationsSize_ = fixedDurations.size();
  std::vector<std::vector<int>> chromaticNotesSet_;
  std::vector<std::vector<int>> fixedDurationsSet_;
  
  void TrainFromSet(const std::vector<std::vector<int>>& trainSet, Eigen::MatrixXd& transitionMatrix);
};

KnowledgeRepresentation* GeneticTraining::Train() {
	std::cout << "Genetic training" << std::endl;
	KnowledgeRepresentation* kw;
	return kw;
}

KnowledgeRepresentation* GeneticTraining::Train(const KnowledgeRepresentation& info) {
	std::cout << "Genetic training + knowledge" << std::endl;
	KnowledgeRepresentation* kw;
	return kw;
}

#endif