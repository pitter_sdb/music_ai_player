#ifndef FixedComposer_HEADER
#define FixedComposer_HEADER

#include "OscOutboundPacketStream.h"
#include "UdpSocket.h"
#include <vector>
#include "MusicImproviser.h"
#include <iostream>
#include <random>
#include <numeric>
#include <algorithm>
#include "SynthesisManager.h"
#include "AIMusicianComposer.h"

class  FixedComposer : public AIMusicianComposer
{
public:
	//RandomComposer(){}
	void Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set, 
		CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations);
	CompositionOutput Compose() {
		CompositionOutput o;
		return o;
	}
};

void FixedComposer::Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set, 
	CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations) {
	
	if (input_set.size() != 0){
		output.notes.push_back(60);
		output.notes.push_back(60);
		output.notes.push_back(60);

		output.durations.push_back(1.0);
		output.durations.push_back(0.5);
		output.durations.push_back(0.25);
	}

	globalNotes.insert(globalNotes.end(), output.notes.begin(), output.notes.end());
	globalDurations.insert(globalDurations.end(), output.durations.begin(), output.durations.end());
}
#endif