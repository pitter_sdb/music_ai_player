//
//  MusicApp.cpp
//  music_ai
//
//  Created by Efrain Astudillo Vargas on 7/31/15.
//  Copyright (c) 2015 Efrain Astudillo. All rights reserved.
//
#include "cinder/gl/gl.h"
#include "MusicAppManager.h"

#include "cinder/ImageIo.h"
#include "cinder/gl/Texture.h"
#include "cinder/Text.h"
#include "cinder/Font.h"
#include "boost/format.hpp"
#include "MusicApp.h"
#include <future>

#ifdef __APPLE__
#define ASSETS_PATH "/Users/eastudillo/Documents/development/tesis/music_ai_player/Music_AI_Player/assets/"
#else
#define ASSETS_PATH "assets/"
#endif
inline float GetRed(unsigned int value) 
{
  auto x2 = value*value;
  auto x3 = value*x2;
  auto x4 = value*x3;
  auto x5 = value*x4;
  float t = (200.3949 - (16.3789*value) - (32.6505*x2) +  (19.8055*x3) - (3.6579*x4) + (0.2053*x5) ) / 255; 
  return t < 0 ? 0 : t > 1.0 ? 1.0 : t;
};
inline float GetGreen(unsigned int value)
{
  auto x2 = value*value;
  auto x3 = value*x2;
  auto x4 = value*x3;
  auto x5 = value*x4;
  float t = (229.2618 - (2.4271*value) - (42.8304*x2) +  (21.9*x3) - (3.3267*x4) + (0.1614*x5) ) / 255;
  return t < 0 ? 0.0f : t > 1.0 ? 1.0f : t;
}
inline float GetBlue(unsigned int value)
{
  auto x2 = value*value;
  auto x3 = value*x2;
  auto x4 = value*x3;
  auto x5 = value*x4;
  float t = (168.5825 - (23.733*value) - (45.985*x2) +  (21.6354*x3) - (3.3663*x4) + (0.1739*x5) ) / 255;
  return t < 0 ? 0 : t > 1.0 ? 1.0 : t;
}

MusicApp::MusicApp() : mMusicManager( new MusicAppManager)
{
  for (unsigned i = 0; i < 5; ++i) 
  {
    mSignalNotes.push_back(std::vector<unsigned int>() );
  }
}
void MusicApp::setup()
{
  /*for (std::vector<std::string>::const_iterator font_name = ci::Font::getNames().begin();
       font_name != ci::Font::getNames().end() ; ++font_name) {
    console() << "Font Name: " << *font_name <<std::endl;
  }*/
  mFont = ci::gl::TextureFont::create(ci::Font("Arial", 10.0f * 2.0f), ci::gl::TextureFont::Format().enableMipmapping());
  try 
  {
    //mImgTexture = ci::gl::Texture( ci::loadImage(ci::app::loadAsset("bacground.jpg") ) );
  } catch (...) 
  {
    console() << "asset background.png not found" << std::endl;
  }

  // create root view
  ci::Rectf frame = ci::Rectf(ci::Vec2i(getWindowWidth() / 2  + 20,80), ci::Vec2i(getWindowWidth() - 50, getWindowHeight() - 60 ));
  inspectorView = Cinder::ControlRoom::View::create(frame);
  
  // connect mouse event listeners, should only be done on the root view
  inspectorView->connectEventListeners();
  
  float value = 0.5f;
  float min = 0.0f;
  float max = 1.0f;
  std::string stringValue = str( boost::format("%.2f") % value);
  
  Cinder::ControlRoom::SliderRef sliders[5];
  Cinder::ControlRoom::LabelRef labels[5];
  Cinder::ControlRoom::LabelRef labelsNamesEmotions[5];
  
  Cinder::ControlRoom::SliderRef slidersVol[5];
  Cinder::ControlRoom::LabelRef labelsVol[5];
  Cinder::ControlRoom::LabelRef labelsVolNames[5];
  
  char* names[] = {"Happiness","Serenity","Sadness","Nostalgia","Passionate"};
  
  
  for (auto i = 0; i < 5; ++i) 
  {
	  float valEmotions = 0.5f;
	  float valVolume = 0.5f;
	  switch (i)
	  {
	  case 0 :
		  valEmotions = 0;
		  valVolume = 0.2;
		  break;
	  case 1:
		  valEmotions = 1.0f;
		  valVolume = 0.1;
		  break;
	  case 2:
		  valEmotions = 0.1;
		  valVolume = 0.5;
		  break;
	  case 3:
		  valEmotions = 1.0f;
		  valVolume = 0.5;
		  break;
	  case 4:
		  valEmotions = 0;
		  valVolume = 0.5;
		  break;
	  default:
		  break;
	  }
  
	  stringValue = str(boost::format("%.2f") % valEmotions);
  

    labels [i] = Cinder::ControlRoom::Label::create( ci::Rectf( (i+1) * 57.0f + 30, 0.0f, 30, 25), stringValue);
	float labelEmotionPosition = i == 0 ? 45 + 20 : (i + 1) * 57.0f + 20;
    labelsNamesEmotions [i] = Cinder::ControlRoom::Label::create( ci::Rectf( labelEmotionPosition, 85.0f, 55, 25), names[i] );
    sliders [i] = Cinder::ControlRoom::Slider::create( ci::Rectf((i+1) * 57.0f + 40, 20.0f, (i+1) * 57.0f + 55.0f, 80.0f), valEmotions, min, max );
    
	stringValue = str(boost::format("%.2f") % valVolume);
    labelsVol [i] = Cinder::ControlRoom::Label::create( ci::Rectf( 0.0f, i * 100, 30, i*100 + 25), stringValue);
    slidersVol [i] = Cinder::ControlRoom::Slider::create( ci::Rectf( 10, i*100+20.0f, 25.0f, i*100+80.0f), valVolume, min, max );
    labelsVolNames[i] =  Cinder::ControlRoom::Label::create( ci::Rectf( 0.0f, i * 100 + 85, 30, i*100 + 95), std::string("Volume") );
    
    inspectorView->addSubview( labels[i] );
    inspectorView->addSubview( labelsNamesEmotions[i] );
    inspectorView->addSubview( sliders[i] );
  
    inspectorView->addSubview( labelsVol[i] );
    inspectorView->addSubview( slidersVol[i] );
    inspectorView->addSubview( labelsVolNames[i] );
    
    // connect value changed event handler
    Cinder::ControlRoom::LabelRef label = labels[i];
    sliders[i]->connectControlEventHandler(
    Cinder::ControlRoom::Control::ControlEvent::ValueChanged, 
         [&, label,i,this ](const Cinder::ControlRoom::ControlRef& control) 
    {
        Cinder::ControlRoom::SliderRef s = std::static_pointer_cast<Cinder::ControlRoom::Slider>(control);
        float value = s->getValue();
		float valuex100 = value * 100.0f;
        //mMusicManager->OuputThreadFuzzy_setVolume(value);
      switch ( i ) {
      case 0:
          mMusicManager->SetAlegria(valuex100); break;
      case 1:
          mMusicManager->SetSerenidad(valuex100); break;
      case 2:
          mMusicManager->SetTristeza(valuex100); break;
      case 3:
          mMusicManager->SetNostalgia(valuex100); break;
      case 4:
          mMusicManager->SetApasionamiento(valuex100); break;
      default:
        break;
      }
        label->setText(str(boost::format("%.2f") % value));
    });
    
    Cinder::ControlRoom::LabelRef _lbltemp = labelsVol[i];
    slidersVol[i]->connectControlEventHandler(
       Cinder::ControlRoom::Control::ControlEvent::ValueChanged, 
       [&, _lbltemp,i,this ](const Cinder::ControlRoom::ControlRef& control) 
       {
         Cinder::ControlRoom::SliderRef s = std::static_pointer_cast<Cinder::ControlRoom::Slider>(control);
         float value = s->getValue();
         
         switch ( i ) {
           case 0:
             mMusicManager->OuputThreadFuzzy_setVolume(value); break;
           case 1:
             mMusicManager->OuputThreadMarkov1_setVolume(value); break;
           case 2:
             mMusicManager->OuputThreadMarkov2_setVolume(value); break;
           case 3:
             if( mMusicManager->GetInputThreadsCollection().size() == 1)
               mMusicManager->GetInputThreadsCollection()[0].SetVolume(value); 
             break;
           case 4:
             if( mMusicManager->GetInputThreadsCollection().size() == 2)
               mMusicManager->GetInputThreadsCollection()[1].SetVolume(value); 
             break;
           default:
             break;
         }
         _lbltemp->setText(str(boost::format("%.2f") % value));
       });
  }
  Cinder::ControlRoom::LabelRef labelBPM = Cinder::ControlRoom::Label::create( ci::Rectf( 185.0f, 170, 30, 25), "Tempo: " + str(boost::format("%d") % 80) );
  Cinder::ControlRoom::SliderRef sliderBPM = Cinder::ControlRoom::Slider::create( ci::Rectf( 205, 180, 220.0f,260.0f), 80, 20, 200 );
  Cinder::ControlRoom::LabelRef labelBPMNames =  Cinder::ControlRoom::Label::create( ci::Rectf( 180.0f,  265, 30, 25), std::string("BPM (Beats per Minute)") );
  sliderBPM->connectControlEventHandler(
    Cinder::ControlRoom::Control::ControlEvent::ValueChanged, 
    [&, labelBPM,this](const Cinder::ControlRoom::ControlRef& control) 
      {
        Cinder::ControlRoom::SliderRef s = std::static_pointer_cast<Cinder::ControlRoom::Slider>(control);
        int value = (int)s->getValue();
        mMusicManager->SetTempo(value);
       labelBPM->setText("Tempo: " + str(boost::format("%.2f") % value));
      });
  inspectorView->addSubview(labelBPM);
  inspectorView->addSubview(sliderBPM);
  inspectorView->addSubview(labelBPMNames);
  
  Cinder::ControlRoom::ButtonRef  button =
  Cinder::ControlRoom::Button::create( ci::Rectf(150,400,250,450) , Cinder::ControlRoom::Button::ButtonType::MomentaryPushIn, "Start");
  inspectorView->addSubview(button);
  
  button->connectControlEventHandler(Cinder::ControlRoom::Control::ControlEvent::Down, 
  [&, this](const Cinder::ControlRoom::ControlRef& control) 
  {
     mMusicManager->Start();
  } );
}

// ===============================  START DRAW    ==========================================
void MusicApp::draw()
{
	if (inspectorView == nullptr) return;
  ci::gl::clear( ci::Color::black() );
  
  ci::gl::color( ci::Color::white() );
  if (mImgTexture) 
    ci::gl::draw(mImgTexture, getWindowBounds());
  
  
  ci::gl::enableAlphaBlending();
  ci::gl::color(0.0f,0.0f, 0.0f,0.7f);
  ci::gl::drawSolidRect(ci::Rectf(20.0f,45.0f, getWindowWidth() - 10.0f, getWindowHeight() - 20.0f ) );
  
  // Textos
  //auto textureText = buildTextureText("Artificial and Human Music Composer", 36 );
  ci::gl::color(ci::Color::white());
  //ci::gl::draw( textureText, 
               //ci::Vec2f( (getWindowWidth() / 2) - (textureText.getWidth()/2), 0));
  //textureText = buildTextureText("Escuela Superior Politécnica del Litoral. Copyright @ 2015", 14 );
  //ci::gl::draw( textureText, 
  //ci::Vec2f( (getWindowWidth() / 2) - (textureText.getWidth()/2), getWindowHeight() - 20));
  //textureText = buildTextureText("Machine Composer", 18 );
  //ci::gl::draw( textureText, 
  //ci::Vec2f( 40 , 50));
  mFont->drawString("Artificial and Human Composer", ci::Vec2f( (getWindowWidth() / 2) - 150, 35), ci::gl::TextureFont::DrawOptions().scale(1.5).pixelSnap(false) );
  mFont->drawString("Machine Composer",  ci::Vec2f( 40 , 68) );
  mFont->drawString("Human Composer", ci::Vec2f( 40 , ((7 * getWindowHeight()/ 12 )+ 28) ));
  //textureText = buildTextureText("Human Composer", 18 );
                    // ci::gl::draw( textureText, 
                    //ci::Vec2f( 40 , ((7 * getWindowHeight()/ 12 )+ 14) ));
  
  ci::gl::disableAlphaBlending();

  ci::gl::color( ci::Color::white() );
  ci::gl::drawLine( ci::Vec2f(40, 72), ci::Vec2f(getWindowWidth() - 35, 72) );
  ci::gl::drawLine( ci::Vec2f(40, (7 * getWindowHeight()/ 12) + 32) , ci::Vec2f(getWindowWidth() - 35, (7 * getWindowHeight()/ 12) + 32) );
  
  std::vector<std::string> NAMEST= {"Fuzzy Logic","Markov Chain Instance 1","Markov Chain Instance 2","Instrument 1", "Instrument 2"};
  
  if(mSignalNotes.size() > 0)
  {
    for ( auto p = 0; p < mSignalNotes.size(); ++p )
    {
      ci::gl::color( ci::Color::white() );
      int _t1;
      int _t2;
      int _t3;
      if(p > 2)
      {
        _t1 = p*90+208;
        _t2 = p*90+130;
        _t3 = p*90 + 200;
      }
      else
      {
        _t1 = p*90+178;
        _t2 = p*90+110;
        _t3 = p*90 + 160;
      }
      ci::gl::drawLine( ci::Vec2f(47, _t1) , ci::Vec2f(47, _t2) );
      ci::gl::drawLine( ci::Vec2f(47, _t1) , ci::Vec2f(400, _t1) );
      
      ci::gl::enableAlphaBlending();
      mFont->drawString(NAMEST[p], ci::Vec2f( 47 , _t2 - 5), ci::gl::TextureFont::DrawOptions().scale(0.9f).pixelSnap(false));
      ci::gl::disableAlphaBlending();
      
      ci::gl::pushMatrices();
      ci::gl::translate( 50, _t3);
      auto &_vec = mSignalNotes[p];
      for ( int i = 0; i < _vec.size(); ++i ) 
      {
        ci::gl::pushMatrices();
        for(int j = 0; j < _vec.at(i) ; ++j)
        {
          ci::gl::color( GetRed(j), GetGreen(j), GetBlue(j) );
          ci::gl::drawSolidRect( ci::Rectf(0,0,5,5) );
          ci::gl::translate(0, -8);
        }
        ci::gl::popMatrices();
        ci::gl::translate(8, 0);
      }
      ci::gl::popMatrices();
    }
  }
  // ========
  inspectorView->draw();
}
// ===============================  END  DRAW    ==========================================
void MusicApp::update()
{
  //console() << getElapsedFrames() << std::endl; //ci::Vec2i(getWindowWidth() / 2  + 50,80), ci::Vec2i(getWindowWidth() - 50, 300)
  //inspectorView->getBounds(). set( getWindowWidth() / 2  + 50, 80, getWindowWidth() - 50, 300);
  if ( mMusicManager->isReady() ) {
    auto fz = mMusicManager->OuputThreadFuzzy_getNotesNumber();
    auto mc1 = mMusicManager->OuputThreadMarkov1_getNotesNumber();
    auto mc2 = mMusicManager->OuputThreadMarkov2_getNotesNumber();
    size_t inst1 = 0,inst2 = 0;
    
    if (mMusicManager->GetInputThreadsCollection().size() == 1)
        inst1 = mMusicManager->GetInputThreadsCollection().at(0).GetNotesNumber();
    else if ( mMusicManager->GetInputThreadsCollection().size() == 2 ) 
    {
        inst1 = mMusicManager->GetInputThreadsCollection().at(0).GetNotesNumber();
        inst2 = mMusicManager->GetInputThreadsCollection().at(1).GetNotesNumber();
    }
    for (unsigned i = 0; i < mSignalNotes.size(); ++i) {
      if (mSignalNotes[i].size() >= 45)
        mSignalNotes[i].erase( mSignalNotes[i].begin() );
      switch (i) {
        case 0:
          mSignalNotes[i].push_back( fz );
          break;
        case 1:
          mSignalNotes[i].push_back( mc1 );
          break;
        case 2:
          mSignalNotes[i].push_back( mc2 );
          break;
        case 3:
          mSignalNotes[i].push_back((unsigned)inst1);
          break;
        case 4:
          mSignalNotes[i].push_back((unsigned)inst2);
          break;
        default:
          break;
      }
    }
  }
}
void MusicApp::shutdown()
{
  //clean up
}
void MusicApp::prepareSettings(cinder::app::AppBasic::Settings *settings)
{
  //ci::app::setFullScreen( true );
  ci::app::addAssetDirectory(ASSETS_PATH);
  
  settings->setTitle("Music Composer");
  settings->setFrameRate(60.0f);
  settings->setWindowSize(800, 600);
  settings->enableHighDensityDisplay();
  //settings->setAlwaysOnTop();
  //settings->setBorderless();
  //settings->setFullScreen();
  //settings->setDisplay(getSecondDisplay());
}
cinder::DisplayRef MusicApp::getSecondDisplay()
{
#ifdef __APPLE__
  CGDirectDisplayID cgId = cinder::Display::getMainDisplay()->getCgDirectDisplayId();
  console() << "MAIN CGid: " << cgId << std::endl;

  for (std::vector<cinder::DisplayRef>::const_iterator displays = 
       cinder::Display::getDisplays().begin(); 
       displays != cinder::Display::getDisplays().end(); ++displays) 
  {
    console() << "CGid: " << (*displays)->getCgDirectDisplayId() << std::endl;
    //if ( cgId != (*displays)->getCgDirectDisplayId()) {
    // return cinder::Display::findFromCgDirectDisplayId((*displays)->getCgDirectDisplayId());
    //}
  }
  //return cinder::Display::findFromCgDirectDisplayId( 458486960 ); // ojo: change with the respective Display ID
  return cinder::Display::getMainDisplay();
#else
  return cinder::Display::getMainDisplay();
#endif
}

void MusicApp::setFuzzyControls() 
{

}
void MusicApp::setChain1Controls()
{

}
void MusicApp::setChain2Controls()
{

}



CINDER_APP_BASIC(MusicApp, ci::app::RendererGl) 