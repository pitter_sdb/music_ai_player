#ifndef FuzzyKnowledgeRepresentation_HEADER
#define FuzzyKnowledgeRepresentation_HEADER


#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include "Eigen/Dense"
#include "KnowledgeRepresentation.h"
#include "red-black.h"

class  FuzzyKnowledgeRepresentation : public  KnowledgeRepresentation {
	

public:

	enum Emotion {
		Alegria,
		Serenidad,
		Tristeza,
		Nostalgia,
		Apasionamiento,
		End,
	};
	struct FuzzyEmotionVar {
		Emotion emotion;
		float degree;//0-100
	};
	struct FuzzyMusicMaterial {
		int keyNote;
		float tempo;
		std::vector<FuzzyEmotionVar> emotionsSet;
		std::vector<MidiUtilities::MidiInfo> melodyPattern;		
	};


	FuzzyKnowledgeRepresentation() {
		emotionBinaryTreesNotes.resize((int)Emotion::End);
		emotionBinaryTreesDurations.resize((int)Emotion::End);
	}

	std::vector<FuzzyMusicMaterial> givenPatterns;// Training directly for humnas
	std::vector<FuzzyMusicMaterial> generatedPatternsNotes;
	std::vector<FuzzyMusicMaterial> generatedPatternsDurations;
	std::vector<FuzzyMusicMaterial> allPatterns;// humans and generated patterns

	std::vector<RedBlackTree<float, int>> emotionBinaryTreesNotes;
	std::vector<RedBlackTree<float, int>> emotionBinaryTreesDurations;

	void Save() {
		WriteInFile("fuzzyDirectPatterns.csv", ";", givenPatterns);
		WriteInFile("fuzzyGeneratedPatternsNotes.csv", ";", generatedPatternsNotes);
		WriteInFile("fuzzyGeneratedPatternsDurations.csv", ";", generatedPatternsDurations);
		WriteInFile("fuzzyAllPatterns.csv", ";", allPatterns);
		//WriteInFile("durationsMatrixTransition.csv", ";", TransitionMatrixDurations);
	}

	void Load() {
		LoadFromFile("fuzzyDirectPatterns.csv", ";", givenPatterns);
		LoadFromFile("fuzzyGeneratedPatternsNotes.csv", ";", generatedPatternsNotes);
		LoadFromFile("fuzzyGeneratedPatternsDurations.csv", ";", generatedPatternsDurations);
		LoadFromFile("fuzzyAllPatterns.csv", ";", allPatterns);

		FillBinaryTrees();
		//LoadFromFile("durationsMatrixTransition.csv", ";", TransitionMatrixDurations);
	}

	void FillBinaryTrees() {

		// FIrst it must be removed
		for (auto i = 0; i < generatedPatternsNotes.size(); ++i) {
			for (auto e = 0; e < Emotion::End; ++e) {
				float keyTree;
				//NOTES
				keyTree = generatedPatternsNotes[i].emotionsSet[e].degree;
				emotionBinaryTreesNotes[e].remove(keyTree);


				//DURATIONS

				keyTree = generatedPatternsDurations[i].emotionsSet[e].degree;
				emotionBinaryTreesDurations[e].remove(keyTree);
			}
		}

		//Second added
		for (auto i = 0; i < generatedPatternsNotes.size(); ++i) {
			for (auto e = 0; e < Emotion::End; ++e) {
				float keyTree;

				//NOTES
				keyTree = generatedPatternsNotes[i].emotionsSet[e].degree;
				emotionBinaryTreesNotes[e].put(keyTree, i);

				//DURATIONS

				keyTree = generatedPatternsDurations[i].emotionsSet[e].degree;
				emotionBinaryTreesDurations[e].put(keyTree, i);
				
			}
		}


	}

private:
	void WriteInFile(std::string fileName, std::string separator, const std::vector<FuzzyMusicMaterial>& fuzzyMusicMaterial);
	void LoadFromFile(std::string fileName, std::string separator, std::vector<FuzzyMusicMaterial>& fuzzyMusicMaterial);

	
};


void FuzzyKnowledgeRepresentation::WriteInFile(std::string fileName, std::string separator, 
	const std::vector<FuzzyMusicMaterial>& fuzzyMusicMaterial) {

	std::ofstream fileStream;
	fileStream.open(fileName);

	for (size_t i = 0; i < fuzzyMusicMaterial.size(); ++i) {

		fileStream << fuzzyMusicMaterial[i].keyNote << separator
			<< fuzzyMusicMaterial[i].tempo << separator;

		for (size_t j = 0; j < fuzzyMusicMaterial[i].emotionsSet.size(); ++j) {
			fileStream << fuzzyMusicMaterial[i].emotionsSet[j].degree << separator;
		}
		for (size_t k = 0; k < fuzzyMusicMaterial[i].melodyPattern.size();++k) {
			fileStream << "(" << fuzzyMusicMaterial[i].melodyPattern[k].note << "," 
				<< fuzzyMusicMaterial[i].melodyPattern[k].duration << ")";
		}
		fileStream << "\n";
	}

	fileStream.close();
};

void FuzzyKnowledgeRepresentation::LoadFromFile(std::string fileName, std::string separator, 
	std::vector<FuzzyMusicMaterial>& fuzzyMusicMaterial) {
	//http://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
	
	std::string line;
	std::ifstream myfile(fileName);
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			std::vector<std::string> valuesStr;
			std::string s = line;
			size_t pos = 0;
			std::string token;
			while ((pos = s.find(separator)) != std::string::npos) {
				token = s.substr(0, pos);
				valuesStr.push_back(token);
				s.erase(0, pos + separator.length());
			}
			valuesStr.push_back(s);
			FuzzyMusicMaterial material;
			material.keyNote = std::stoi(valuesStr[0]);
			material.tempo = std::stof(valuesStr[1]);
			int i;
			for (i = 0; i < Emotion::End; ++i) {
				FuzzyEmotionVar emotionVar;
				emotionVar.emotion = (Emotion) (i);
				emotionVar.degree =  std::stof(valuesStr[i + 2]);
				material.emotionsSet.push_back(emotionVar);
			}

			auto melodyStr = valuesStr[i + 2];
			std::string melodySeparator(")");
			std::string paramSeparator(",");
			while ((pos = melodyStr.find(melodySeparator)) != std::string::npos) {
				MidiUtilities::MidiInfo info;
				token = melodyStr.substr(1, pos);
				size_t posMel;
				while ((posMel = token.find(paramSeparator)) != std::string::npos) {
					std::string param;
					param = token.substr(0, posMel);
					info.note = std::stoi(param);
					token.erase(0, posMel + paramSeparator.length());
				}
				token = token.substr(0, posMel - 1);
				info.duration = std::stof(token);
				valuesStr.push_back(token);
				melodyStr.erase(0, pos + melodySeparator.length());
				material.melodyPattern.push_back(info);
			}
			fuzzyMusicMaterial.push_back(material);
		}
		myfile.close();
	}
};

#endif