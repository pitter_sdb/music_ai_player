#ifndef FuzzyComposer_HEADER
#define FuzzyComposer_HEADER

#include "Eigen/Dense"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>
#include <vector>
#include "KnowledgeRepresentation.h"
#include "AIMusicianComposer.h"
#include "MidiUtilities.h"
#include "FuzzyKnowledgeRepresentation.h"
#include "red-black.h"
#include <set>
#include <math.h>       /* sqrt */
#include <limits>

using namespace MidiUtilities;

class  FuzzyComposer : public  AIMusicianComposer {
public:


	FuzzyComposer(FuzzyKnowledgeRepresentation * knowledge) {
		knowledge_ = knowledge;
	}

	void SetEmotionDegress(const std::vector<float> emotionsDegrees) {
		emotionsDegrees_ = emotionsDegrees;
	}

	std::vector<float> GetEmotionsDegrees() {
		return emotionsDegrees_;
	}

	CompositionOutput Compose() {
		CompositionOutput output;
		return output;
	}

	void Apply(MusicOperation::MUSICAL_NOTE keyNote, const std::vector<MidiInfo> &input_set,
		CompositionOutput& output, vector<int>& globalNotes, vector<float>& globalDurations) {

		std::vector<MidiInfo> normalizedInputSet(input_set);

		//Normalize notes 
		for (size_t i = 0; i < normalizedInputSet.size(); i++)
		{
			normalizedInputSet[i].note = normalizedInputSet[i].note - keyNote;
		}

		indexPatternsNotes_.clear();
		for (size_t i = 0; i < emotionsDegrees_.size(); ++i) {
			SearchInTree(&knowledge_->emotionBinaryTreesNotes[i], emotionsDegrees_[i], indexPatternsNotes_);
			SearchInTree(&knowledge_->emotionBinaryTreesDurations[i], emotionsDegrees_[i], indexPatternsDurations_);
		}
		
		//int c = 0;
		float minEmotionDistanceNote = std::numeric_limits<float>::infinity();
		std::vector<MidiInfo> * targetPatternNote = nullptr;
		for (auto index : indexPatternsNotes_) {
			//std::cout << "Key Note: " << c<<", "<< knowledge_->generatedPatternsNotes[index].emotionsSet[0].degree << endl;
			//c++;
			float emotionDistance = GetEmotionDistance(knowledge_->generatedPatternsNotes[index].emotionsSet, emotionsDegrees_);
			if (emotionDistance < minEmotionDistanceNote && IsConsistentMelodyWithHarmomy(knowledge_->generatedPatternsNotes[index].melodyPattern, normalizedInputSet)) {
				minEmotionDistanceNote = emotionDistance;
				targetPatternNote = &knowledge_->generatedPatternsNotes[index].melodyPattern;
				//std::cout << "Min: " << minEmotionDistance << endl;
			}
		}


		float minEmotionDistanceDuration = std::numeric_limits<float>::infinity();
		std::vector<MidiInfo> * targetPatternDuration = nullptr;
		for (auto index : indexPatternsDurations_) {
			//std::cout << "Key Note: " << c<<", "<< knowledge_->generatedPatternsNotes[index].emotionsSet[0].degree << endl;
			//c++;
			float emotionDistance = GetEmotionDistance(knowledge_->generatedPatternsDurations[index].emotionsSet, emotionsDegrees_);
			if (emotionDistance < minEmotionDistanceDuration) {
				minEmotionDistanceDuration = emotionDistance;
				targetPatternDuration = &knowledge_->generatedPatternsDurations[index].melodyPattern;
				//std::cout << "Min: " << minEmotionDistance << endl;
			}
		}

		output.notes.clear();
		output.durations.clear();
		if (targetPatternNote != nullptr && targetPatternDuration != nullptr) {

			int minVal = 1;
			int maxVal = input_set.size();

			int size =  rand() % (maxVal - minVal + 1) + minVal;
			size = (std::min)(size, (int)targetPatternNote->size());
			for (size_t i = 0; i < size; i++) {
				//std::cout << "Choosen Pattern Note: " << minEmotionDistance << ", " << (*targetPattern)[i].note << endl;
				int note = (*targetPatternNote)[i].note;
				float duration = (*targetPatternDuration)[i].duration;
				output.notes.push_back(note + keyNote);
				output.durations.push_back(duration);
				globalNotes.push_back(note);
				globalDurations.push_back(duration);
			}
		}
	}

private:
	FuzzyKnowledgeRepresentation * knowledge_;
	std::vector<float> emotionsDegrees_;
	std::set<int> indexPatternsNotes_;
	std::set<int> indexPatternsDurations_;

	void SearchInTree(RedBlackTree<float, int> * tree, float emotionDegree, std::set<int>& indexPatterns) {
		RedBlackTree<float, int>::Node * result = nullptr;
		RedBlackTree<float, int>::Node * parentResult = nullptr;

		tree->getNearest(emotionDegree, &result, &parentResult);

		std::string resultStr;
		
		RedBlackTree<float, int>::Node * pTarget = parentResult;

		if (result == nullptr) {
			resultStr = "Null";
		} else {
			resultStr = "Patterns: " + std::to_string(result->valuesSet.size());
			pTarget = result;
		}

		for (size_t i = 0; i < pTarget->valuesSet.size(); i++) {
			indexPatterns.insert(pTarget->valuesSet[i]);
		}

		if (pTarget->parent != nullptr) {
			for (size_t i = 0; i < pTarget->parent->valuesSet.size(); i++) {	
				indexPatterns.insert(pTarget->parent->valuesSet[i]);
			}
		}

		if (pTarget->left != nullptr) {
			for (size_t i = 0; i < pTarget->left->valuesSet.size(); i++) {
				indexPatterns.insert(pTarget->left->valuesSet[i]);
			}
		}

		if (pTarget->right != nullptr) {
			for (size_t i = 0; i < pTarget->right->valuesSet.size(); i++) {
				indexPatterns.insert(pTarget->right->valuesSet[i]);
			}
		}

		//std::cout << "SEARCHING: " << resultStr << ", " << parentResult->key << ", " << indexPatternsNotes_.size() <<endl;
	}
	

	float GetEmotionDistance(const std::vector<FuzzyKnowledgeRepresentation::FuzzyEmotionVar>& emotionSetKwBase, const std::vector<float>& emotionSetHumanPerformer) {
		float distance = 0;
		for (size_t i = 0; i < emotionSetKwBase.size(); i++) {
			float diff = emotionSetKwBase[i].degree - emotionSetHumanPerformer[i];
			distance += (diff * diff);
		}
		distance = sqrtf(distance);
		return distance;
	}

	bool IsConsistentMelodyWithHarmomy(const std::vector<MidiInfo> &input_set_kwbase, const std::vector<MidiInfo> &input_set_humanPerformer) {
		// Criteria: One of the notes in input_set_humanPerformer is teh first note in input_set_kwbase

		for (size_t i = 0; i < input_set_humanPerformer.size(); ++i) {
			if (input_set_kwbase.size() > 0){
				if (input_set_humanPerformer[i].note % 12 == input_set_kwbase[0].note % 12)
					return true;
			}
		}
		return false;
		
	}
};

#endif