#ifndef BasicMidiKnowledgeRepresentation_HEADER
#define BasicMidiKnowledgeRepresentation_HEADER

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include "Eigen/Dense"
#include "KnowledgeRepresentation.h"
#include "MidiUtilities.h"

// It is assumed that teh key note is C or Am and durations are relative
class  BasicMidiKnowledgeRepresentation : public  KnowledgeRepresentation {
public:

	std::vector<std::vector<MidiUtilities::MidiInfo>> midiInfoSequences;

	void Save() {

	}

	void Load() {

	}
private:
	
};


#endif