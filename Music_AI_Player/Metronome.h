//Timer from: http://cc.byexamples.com/2007/03/14/create-a-timer/

#ifndef METRONOME_HEADER
#define METRONOME_HEADER

#include <ctime>
#include <iostream>
#include <chrono>

typedef  std::chrono::milliseconds SECONDS;


using namespace std;

class Metronome;
void defaultOnBeat(){ ; }

class Metronome{
private:
  //unsigned long startTime_;
  std::chrono::time_point<std::chrono::system_clock> startTime_;
	bool done_;
	float deltaTime_;//in seconds
	float actualTime_;
	std::thread helperThread_;	

public:
  /* that is good but is not full supported yet
   * tr1 = Technical Report 1
   */
  //typedef std::tr1::function<void()> OnBeatHandler;
  
  //Metronome(int BPM, OnBeatHandler bh = defaultOnBeat): onBeatHandlerFunc_(bh) {
  Metronome(int BPM, void (*pointer_func) () ): onBeatHandlerFunc_(pointer_func) {
		deltaTime_ = 60.0f / BPM;		
		std::cout << "Metronome Delta Time: "<< deltaTime_ << std::endl;
	};

	void Start(){
		done_ = false;
    //startTime_ = clock();
    startTime_ = std::chrono::system_clock::now();
	};

	void Finish(){
		done_ = true;
	};

	bool IsPlaying(){
		return !done_;
	};

	void Update(){
    
		while (!done_){
		  //unsigned long clockCurrent = clock();
				auto clockCurrent = std::chrono::system_clock::now();
		  auto currentTime = std::chrono::duration_cast<SECONDS>
		  ( clockCurrent - startTime_);
      
				if (currentTime.count() >= deltaTime_ * 1000){				
			//std::cout<< "Clock: " << currentTime.count() << std::endl;
					onBeatHandlerFunc_();
			//std::cout<< "StartTime: " << startTime_ << " Delta Time: "<< deltaTime_<< std::endl;
					startTime_ = clockCurrent;
				}
#if defined(__WINDOWS_MM__)
				Sleep(30);
#elif defined(__MACOSX_CORE__)
				sleep(30);
#endif
		}
	};

	void SetTempo(int BPM) {
		deltaTime_ = 60.0f / BPM;
		std::cout << "Meronome Delta Time: " << deltaTime_ << std::endl;
	}
private:
  //OnBeatHandler onBeatHandlerFunc_;
  void (*onBeatHandlerFunc_)();
};

#endif


