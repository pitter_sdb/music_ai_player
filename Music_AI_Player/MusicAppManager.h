
#ifndef MusicAppManager_HEADER
#define MusicAppManager_HEADER

#include "RealTimeMusicalPerformance.h"
#include <future>

void OnBeatPerformUpdate();
RealTimeMusicalPerformance realTimeMusicalHandler_(80, MusicOperation::MUSICAL_NOTE::C, OnBeatPerformUpdate);

class  MusicAppManager {
public:
  MusicAppManager() : mInitialized(false) {
		
	};

  ~MusicAppManager(){
    if(mThread.joinable()) mThread.join();
  }
	void StartSynthesisEngine() {
		realTimeMusicalHandler_.StartSynthesisEngine();
	}

	void StopSynthesisEngine() {
		realTimeMusicalHandler_.StopSynthesisEngine();
	}

	void SetTempo(int BPM) {
		realTimeMusicalHandler_.SetBPM(BPM);
	}

	void SetKeyNote(MusicOperation::MUSICAL_NOTE keyNote) {
		realTimeMusicalHandler_.SetKeyNote(keyNote);
	}

	void Start() {
    if (!mInitialized)
      mThread =  std::thread(&MusicAppManager::__Start, this );
    
    mInitialized = true;
	}

	// SET Emotions
	void SetAlegria(float value) {
		realTimeMusicalHandler_.SetEmotionValue(0, value);
	}

	void SetSerenidad(float value) {
		realTimeMusicalHandler_.SetEmotionValue(1, value);
	}

	void SetTristeza(float value) {
		realTimeMusicalHandler_.SetEmotionValue(2, value);
	}

	void SetNostalgia(float value) {
		realTimeMusicalHandler_.SetEmotionValue(3, value);
	}

	void SetApasionamiento(float value) {
		realTimeMusicalHandler_.SetEmotionValue(4, value);
	}


	// GET Emotions
	float GetAlegria() const {
		return realTimeMusicalHandler_.GetEmotionValue(0);
	}

	float GetSerenidad() const  {
		return realTimeMusicalHandler_.GetEmotionValue(1);
	}

	float GetTristeza() const {
		return realTimeMusicalHandler_.GetEmotionValue(2);
	}

	float GetNostalgia() const {
		return realTimeMusicalHandler_.GetEmotionValue(3);
	}

	float GetApasionamiento() const {
		return realTimeMusicalHandler_.GetEmotionValue(4);
	}

	//Threads info

	//Set Volumes OUput
	void OuputThreadMarkov1_setVolume(float value) {
    if( realTimeMusicalHandler_.OutputThreadsInfoCollection.size()>0 &&  realTimeMusicalHandler_.OutputThreadsInfoCollection[0].synthesizer )
      realTimeMusicalHandler_.OutputThreadsInfoCollection[0].synthesizer->SetVolume(value);
	}

	void OuputThreadMarkov2_setVolume(float value) {
    if( realTimeMusicalHandler_.OutputThreadsInfoCollection.size()>0 && realTimeMusicalHandler_.OutputThreadsInfoCollection[1].synthesizer )
      realTimeMusicalHandler_.OutputThreadsInfoCollection[1].synthesizer->SetVolume(value);
	}

	void OuputThreadFuzzy_setVolume(float value) {
    if( realTimeMusicalHandler_.OutputThreadsInfoCollection.size()>0 &&  realTimeMusicalHandler_.OutputThreadsInfoCollection[2].synthesizer )
      realTimeMusicalHandler_.OutputThreadsInfoCollection[2].synthesizer->SetVolume(value);
	}

	//Get Volumes OUput
	float OuputThreadMarkov1_getVolume() const {
    return !realTimeMusicalHandler_.OutputThreadsInfoCollection[0].synthesizer ? realTimeMusicalHandler_.OutputThreadsInfoCollection[0].synthesizer->GetVolume() : 0.0f;
	}

	float OuputThreadMarkov2_getVolume() const {
		return !realTimeMusicalHandler_.OutputThreadsInfoCollection[1].synthesizer ? realTimeMusicalHandler_.OutputThreadsInfoCollection[1].synthesizer->GetVolume() : 0.0f;
	}

	float OuputThreadFuzzy_getVolume() const {
		return !realTimeMusicalHandler_.OutputThreadsInfoCollection[2].synthesizer ? realTimeMusicalHandler_.OutputThreadsInfoCollection[2].synthesizer->GetVolume() : 0.0f;
	}

	//Get Notes Numbers OUput
	int OuputThreadMarkov1_getNotesNumber() const {
    return realTimeMusicalHandler_.OutputThreadsInfoCollection[0].notesNumber;
    //return 8;
	}

	int OuputThreadMarkov2_getNotesNumber() const {
    return realTimeMusicalHandler_.OutputThreadsInfoCollection[1].notesNumber;
    //return 7;
	}

	int OuputThreadFuzzy_getNotesNumber() const {
    return realTimeMusicalHandler_.OutputThreadsInfoCollection[2].notesNumber;
    //return 4;
	}

	// Collection for Inputs 

	std::vector<InputThreadInfo>& GetInputThreadsCollection() {
		return realTimeMusicalHandler_.InputThreadsInfoCollection;
	}


	void Finish() {
		realTimeMusicalHandler_.Finish();
	}
  bool isReady() const { return realTimeMusicalHandler_.isInitialized(); }
private:
  std::thread mThread;
  bool mInitialized;
  void __Start(){
#ifdef __WINDOWS_MM__
	  StartSynthesisEngine();
#endif
   realTimeMusicalHandler_.ExecutePerformance();
  }
};

void OnBeatPerformUpdate() {//UPADTE FUNCTION
	realTimeMusicalHandler_.OnBeatPerform();
}


#endif